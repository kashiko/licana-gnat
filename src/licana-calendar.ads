with Ada.Calendar; use Ada.Calendar;
with Ada.Calendar.Formatting;

package Licana.Calendar is

   Unix_Epoch : constant Time :=
     Formatting.Time_Of (Year => 1970, Month => 1, Day => 1);

end Licana.Calendar;
