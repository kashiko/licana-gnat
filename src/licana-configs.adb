with Ada.Characters.Latin_1; use Ada.Characters.Latin_1;
with Ada.Strings.Unbounded;  use Ada.Strings.Unbounded;
with GNATCOLL.Config;        use GNATCOLL.Config;
with Licana.Streams;         use Licana.Streams;
with Licana.Text_IO;         use Licana.Text_IO;
with Licana.URL_Utils;       use Licana.URL_Utils;

package body Licana.Configs is

   ----------
   -- Read --
   ----------

   procedure Read (Path : String; Context : in out Contexts.Context) is
      Pool   : Config_Pool;
      Parser : INI_Parser;

      procedure Set (Variable : in out Unbounded_String; Key : String;
         Section              :        String)
      is
         S : String := Get (Pool, Key, Section);
      begin
         if S'Length > 0 then
            Variable := To_Unbounded_String (S);
         end if;
      end Set;

      procedure Set (Variable : in out Boolean; Key : String; Section : String)
      is
         S : String := Get (Pool, Key, Section);
      begin
         if S'Length > 0 then
            Variable := Get_Boolean (Pool, Key, Section);
         end if;
      end Set;

      procedure Set (Variable : in out URL_Scheme_Vectors.Vector; Key : String;
         Section              :        String)
      is
         Schemes : URL_Scheme_Vectors.Vector;
         I       : Positive := 1;
         Found   : Boolean  := False;
      begin
         loop
            declare
               Item : String := Get (Pool, Key, Section, I);
            begin
               exit when Item'Length < 1;
               Schemes.Append (Item);
               Found := True;
            end;
            I := I + 1;
         end loop;
         if Found then
            Variable := Schemes;
         end if;
      end Set;
   begin
      Open (Parser, Path);
      Fill (Pool, Parser);

      Set (Context.Conversion.Embedded, "Embedded", "Conversion");
      Set
        (Context.Conversion.Style_Sheet_URL, "Style_Sheet_URL", "Conversion");
      Set
        (Context.Conversion.Tilde_Escape_In_Headings_Enabled,
         "Tilde_Escape_In_Headings_Enabled", "Conversion");
      Set
        (Context.Conversion.Additions_Enabled, "Additions_Enabled",
         "Conversion");
      Set (Context.Conversion.Indent_Style, "Indent_Style", "Conversion");
      Set
        (Context.Conversion.Heading_Anchors_Enabled, "Heading_Anchors_Enabled",
         "Conversion");

      Set
        (Context.Format.Null_Contents_Enabled, "Null_Contents_Enabled",
         "Format");
      Set (Context.Format.URL_Schemes, "URL_Schemes", "Format");
      Set (Context.Format.Additions_Enabled, "Additions_Enabled", "Format");
      Set
        (Context.Format.Index_Filename_Omission_Enabled,
         "Index_Filename_Omission_Enabled", "Format");
      Set (Context.Format.Document_Root, "Document_Root", "Format");
      Set (Context.Format.MathJax_Enabled, "MathJax_Enabled", "Format");
      Set
        (Context.Format.URL_Fragment_Prefix_Omission_Enabled,
         "URL_Fragment_Prefix_Omission_Enabled", "Format");
   end Read;

   -----------
   -- Write --
   -----------

   procedure Write (Stream : Stream_Access; Context : Contexts.Context) is
      Buffer_Size   : constant := 512;
      Output_Stream : aliased Buffered_Stream_Type (Stream, Buffer_Size);
      Buffer        : Unbounded_String;

      function To_String (Value : Boolean) return String is
      begin
         if Value then
            return "True";
         else
            return "False";
         end if;
      end To_String;

      function To_String (Schemes : URL_Scheme_Vectors.Vector) return String is
         Buffer : Unbounded_String;
         First  : Boolean := True;
      begin
         for Scheme of Schemes loop
            if not First then
               Append (Buffer, ',');
            end if;
            Append (Buffer, Scheme);
            First := False;
         end loop;
         return To_String (Buffer);
      end To_String;

   begin
      Append (Buffer, "[Conversion]" & LF);
      Append (Buffer, LF);
      Append
        (Buffer, "Embedded = " & To_String (Context.Conversion.Embedded) & LF);
      Append
        (Buffer,
         "Style_Sheet_URL = " & Context.Conversion.Style_Sheet_URL & LF);
      Append (Buffer, LF);
      Append
        (Buffer,
         "Tilde_Escape_In_Headings_Enabled = " &
         To_String (Context.Conversion.Tilde_Escape_In_Headings_Enabled) & LF);
      Append
        (Buffer,
         "Additions_Enabled = " &
         To_String (Context.Conversion.Additions_Enabled) & LF);
      Append
        (Buffer,
         "Indent_Style = " & To_String (Context.Conversion.Indent_Style) & LF);
      Append
        (Buffer,
         "Heading_Anchors_Enabled = " &
         To_String (Context.Conversion.Heading_Anchors_Enabled) & LF);

      Append (Buffer, LF);

      Append (Buffer, "[Format]" & LF);
      Append (Buffer, LF);
      Append
        (Buffer,
         "Null_Contents_Enabled = " &
         To_String (Context.Format.Null_Contents_Enabled) & LF);
      Append
        (Buffer,
         "URL_Schemes = " & To_String (Context.Format.URL_Schemes) & LF);
      Append
        (Buffer,
         "Additions_Enabled = " &
         To_String (Context.Format.Additions_Enabled) & LF);
      Append
        (Buffer,
         "Index_Filename_Omission_Enabled = " &
         To_String (Context.Format.Index_Filename_Omission_Enabled) & LF);
      Append
        (Buffer,
         "Document_Root = " & To_String (Context.Format.Document_Root) & LF);
      Append
        (Buffer,
         "MathJax_Enabled = " & To_String (Context.Format.MathJax_Enabled) &
         LF);
      Append
        (Buffer,
         "URL_Fragment_Prefix_Omission_Enabled = " &
         To_String (Context.Format.URL_Fragment_Prefix_Omission_Enabled) & LF);

      Put (Output_Stream, Buffer);
      Flush (Output_Stream);
   end Write;

end Licana.Configs;
