with Ada.Streams.Stream_IO; use Ada.Streams.Stream_IO;
with Licana.Contexts;       use Licana.Contexts;

package Licana.Configs is

   procedure Read (Path : String; Context : in out Contexts.Context);

   procedure Write (Stream : Stream_Access; Context : Contexts.Context);

end Licana.Configs;
