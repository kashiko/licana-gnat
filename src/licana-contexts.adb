package body Licana.Contexts is

   ------------------
   -- Set_Defaults --
   ------------------

   procedure Set_Defaults (Config : in out Format_Config) is
   begin
      Config.URL_Schemes.Append ("http");
      Config.URL_Schemes.Append ("ftp");
      Config.URL_Schemes.Append ("https");
      Config.URL_Schemes.Append ("file");
   end Set_Defaults;

   ----------
   -- Copy --
   ----------

   function Copy (Config : Format_Config) return Format_Config is
   begin
      return R : Format_Config := Config do
         R.URL_Schemes := URL_Scheme_Vectors.Copy (Config.URL_Schemes);
      end return;
   end Copy;

   ------------------
   -- Set_Defaults --
   ------------------

   procedure Set_Defaults (Config : in out Conversion_Config) is
   begin
      null;
   end Set_Defaults;

   ----------
   -- Copy --
   ----------

   function Copy (Config : Conversion_Config) return Conversion_Config is
   begin
      return R : Conversion_Config := Config;
   end Copy;

   ------------------
   -- Set_Defaults --
   ------------------

   procedure Set_Defaults (C : in out Context) is
   begin
      Set_Defaults (C.Format);
      Set_Defaults (C.Conversion);
   end Set_Defaults;

   ----------
   -- Copy --
   ----------

   function Copy (C : Context) return Context is
   begin
      return R : Context := C do
         R.Format     := Copy (C.Format);
         R.Conversion := Copy (C.Conversion);
      end return;
   end Copy;

end Licana.Contexts;
