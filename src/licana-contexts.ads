with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with Licana.URL_Utils;      use Licana.URL_Utils;

package Licana.Contexts is

   type Format_Config is record
      Null_Contents_Enabled           : Boolean          := False;
      URL_Schemes                     : URL_Scheme_Vectors.Vector;
      Additions_Enabled               : Boolean          := False;
      Index_Filename_Omission_Enabled : Boolean          := False;
      Document_Root                   : Unbounded_String :=
        To_Unbounded_String ("https://example.org/");
      MathJax_Enabled                      : Boolean := False;
      URL_Fragment_Prefix_Omission_Enabled : Boolean := False;
   end record;

   procedure Set_Defaults (Config : in out Format_Config);
   function Copy (Config : Format_Config) return Format_Config;

   type Conversion_Config is record
      Embedded        : Boolean          := False;
      Style_Sheet_URL : Unbounded_String := To_Unbounded_String ("style.css");

      Tilde_Escape_In_Headings_Enabled : Boolean          := False;
      Additions_Enabled                : Boolean          := False;
      Indent_Style                     : Unbounded_String :=
        To_Unbounded_String ("class=""indent""");
      Heading_Anchors_Enabled : Boolean := False;
   end record;

   procedure Set_Defaults (Config : in out Conversion_Config);
   function Copy (Config : Conversion_Config) return Conversion_Config;

   type Context is record
      Format      : Format_Config;
      Conversion  : Conversion_Config;
      Title       : Unbounded_String;
      Title_Level : Natural := 0;
   end record;

   procedure Set_Defaults (C : in out Context);
   function Copy (C : Context) return Context;

end Licana.Contexts;
