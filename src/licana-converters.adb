with Ada.Characters.Latin_1; use Ada.Characters.Latin_1;
with Ada.Containers.Vectors;
with Ada.IO_Exceptions;      use Ada.IO_Exceptions;
with Ada.Streams;            use Ada.Streams;
with Licana.Formatters;      use Licana.Formatters;
with Licana.HTML_Utils;      use Licana.HTML_Utils;
with Licana.Streams;         use Licana.Streams;
with Licana.Strings;         use Licana.Strings;
with Licana.Text_IO;         use Licana.Text_IO;

package body Licana.Converters is

   -------------------
   -- Parse_Heading --
   -------------------

   procedure Parse_Heading (Line :     Unbounded_String; Level : out Natural;
      Heading                    : out Unbounded_String; Escape : Boolean)
   is
      L : Natural := 0;
      I : Natural := 1;
   begin
      loop
         if I > Length (Line) then
            Level := 0;
            return;
         end if;
         exit when not Is_Whitespace (Element (Line, I));
         I := I + 1;
      end loop;
      loop
         exit when Element (Line, I) /= '=';
         L := L + 1;
         I := I + 1;
         if I > Length (Line) then
            -- Reject when only '='s in the line.
            Level := 0;
            return;
         end if;
      end loop;
      if L < 1 or else L > 6 then
         Level := 0;
         return;
      end if;
      loop
         exit when not Is_Whitespace (Element (Line, I));
         I := I + 1;
         if I > Length (Line) then
            Level := 0;
            return;
         end if;
      end loop;

      declare
         J : Natural := I;
         K : Natural := I;
         type State is (Body_State, End_Mark_State, End_Space_State);
         S    : State   := Body_State;
         Last : Natural := I;
      begin
         loop
            if Escape and then Is_Tilde_Escaping (Line, K, Length (Line)) then
               K    := K + 1;
               J    := K;
               S    := Body_State;
               Last := K;
            else
               declare
                  C : Character := Element (Line, K);
               begin
                  case S is
                     when Body_State =>
                        if Is_Whitespace (C) then
                           null;
                        elsif C = '=' then
                           S    := End_Mark_State;
                           Last := K;
                        else
                           J    := K;
                           Last := K;
                        end if;
                     when End_Mark_State =>
                        if Is_Whitespace (C) then
                           S := End_Space_State;
                        elsif C = '=' then
                           Last := K;
                        else
                           S    := Body_State;
                           J    := K;
                           Last := K;
                        end if;
                     when End_Space_State =>
                        if Is_Whitespace (C) then
                           null;
                        elsif C = '=' then
                           S    := End_Mark_State;
                           J    := Last;
                           Last := K;
                        else
                           S    := Body_State;
                           J    := K;
                           Last := K;
                        end if;
                  end case;
               end;
            end if;
            K := K + 1;
            exit when K > Length (Line);
         end loop;

         Level := L;
         if Escape then
            Heading := Tilde_Unescape (Line, I, J);
         else
            Heading := Unbounded_Slice (Line, I, J);
         end if;
      end;
   end Parse_Heading;

   -------------
   -- Convert --
   -------------

   procedure Convert (Context : in out Contexts.Context;
      To_Path                 :        access function
        (Name : Unbounded_String) return Unbounded_String;
      Inter_Map : Inter_Maps.Inter_Map; Text_Input_Stream : Stream_Access;
      HTML_Output_Stream : Stream_Access)
   is
      Buffer_Size  : constant := 512;
      Input_Stream : Buffered_Stream_Type (Text_Input_Stream, Buffer_Size);

      type Block_Mode is
        (No_Block, Paragraph_Block, List_Block, Table_Block, Nowiki_Block,
         Indented_Paragraph_Block, Description_List_Block);
      Mode : Block_Mode := No_Block;

      type List_Kind is (Unordered_List, Ordered_List);
      package List_Stack_Vectors is new Ada.Containers.Vectors (Positive,
         List_Kind);
      use List_Stack_Vectors;
      List_Stack : Vector;

      Line             : Unbounded_String;
      Paragraph_Buffer : Unbounded_String;
      HTML_Buffer      : Unbounded_String;

      Indent_Level : Natural := 0;

      ----------------
      -- Open_Block --
      ----------------

      procedure Open_Block (New_Mode : Block_Mode) is
      begin
         case New_Mode is
            when No_Block =>
               null;
            when Paragraph_Block =>
               null;
            when List_Block =>
               null;
            when Table_Block =>
               Append (HTML_Buffer, "<table>" & LF);
            when Nowiki_Block =>
               Append (HTML_Buffer, "<pre>" & LF);
            when Indented_Paragraph_Block =>
               null;
            when Description_List_Block =>
               Append (HTML_Buffer, "<dl>" & LF);
         end case;

         Mode := New_Mode;
      end Open_Block;

      -------------------
      -- Append_Indent --
      -------------------

      procedure Append_Indent (Level : Natural) is
      begin
         for I in 2 .. Level loop
            Append (HTML_Buffer, "  ");
         end loop;
      end Append_Indent;

      ------------------------
      -- Append_List_Indent --
      ------------------------

      procedure Append_List_Indent is
      begin
         Append_Indent (Natural (Length (List_Stack)));
      end Append_List_Indent;

      ----------------------
      -- Append_List_Open --
      ----------------------

      procedure Append_List_Open (Kind : List_Kind) is
      begin
         Append_List_Indent;
         case Kind is
            when Unordered_List =>
               Append (HTML_Buffer, "<ul>" & LF);
            when Ordered_List =>
               Append (HTML_Buffer, "<ol>" & LF);
         end case;
      end Append_List_Open;

      ------------------------
      -- Append_List_Closed --
      ------------------------

      procedure Append_List_Closed is
      begin
         Append_List_Indent;
         case Last_Element (List_Stack) is
            when Unordered_List =>
               Append (HTML_Buffer, "</ul>" & LF);
            when Ordered_List =>
               Append (HTML_Buffer, "</ol>" & LF);
         end case;
      end Append_List_Closed;

      ---------------------
      -- Flush_Paragraph --
      ---------------------

      procedure Flush_Paragraph is
      begin
         Append
           (HTML_Buffer,
            Format (Context, To_Path, Inter_Map, Paragraph_Buffer));
         Append (HTML_Buffer, LF);
         Paragraph_Buffer := Null_Unbounded_String;
      end Flush_Paragraph;

      ----------------------------
      -- Flush_Description_List --
      ----------------------------

      procedure Flush_Description_List is
         Bar_Index         : Natural;
         Closed_Mark_Index : Natural;
         Formatted         : Unbounded_String :=
           Find_And_Format
             (Context, To_Path, Inter_Map, Paragraph_Buffer, 1,
              Length (Paragraph_Buffer), True, ':', Bar_Index, "",
              Closed_Mark_Index);
      begin
         if Bar_Index < 1 then
            Append (HTML_Buffer, "<dt>");
            Append (HTML_Buffer, Trim_Whitespaces_And_Newlines (Formatted));
            Append (HTML_Buffer, "</dt>" & LF);
         else
            Append (HTML_Buffer, "<dt>");
            Append (HTML_Buffer, Trim_Whitespaces_And_Newlines (Formatted));
            Append (HTML_Buffer, "</dt>" & LF);
            loop
               declare
                  Next_Bar_Index : Natural;
               begin
                  Formatted :=
                    Find_And_Format
                      (Context, To_Path, Inter_Map, Paragraph_Buffer,
                       Bar_Index + 1, Length (Paragraph_Buffer), True, ':',
                       Next_Bar_Index, "", Closed_Mark_Index);
                  if Next_Bar_Index < 1 then
                     Next_Bar_Index := Length (Paragraph_Buffer) + 1;
                  end if;
                  if Bar_Index + 1 < Next_Bar_Index
                    or else Context.Format.Null_Contents_Enabled then
                     Append (HTML_Buffer, "<dd>");
                     if Bar_Index + 1 < Next_Bar_Index then
                        Append
                          (HTML_Buffer,
                           Trim_Whitespaces_And_Newlines (Formatted));
                     end if;
                     Append (HTML_Buffer, "</dd>" & LF);
                  end if;
                  Bar_Index := Next_Bar_Index;
                  exit when Bar_Index > Length (Paragraph_Buffer);
               end;
            end loop;
         end if;
         Paragraph_Buffer := Null_Unbounded_String;
      end Flush_Description_List;

      -----------------
      -- Close_Block --
      -----------------

      procedure Close_Block is
      begin
         case Mode is
            when No_Block =>
               null;
            when Paragraph_Block =>
               declare
                  Formatted : Unbounded_String :=
                    Format (Context, To_Path, Inter_Map, Paragraph_Buffer);
               begin
                  if Length (Formatted) > 0 then
                     Append (HTML_Buffer, "<p>" & LF);
                     Append (HTML_Buffer, Formatted);
                     Append (HTML_Buffer, LF);
                     Append (HTML_Buffer, "</p>" & LF);
                  end if;
                  Paragraph_Buffer := Null_Unbounded_String;
               end;
            when List_Block =>
               if Length (Paragraph_Buffer) > 0 then
                  Append
                    (HTML_Buffer,
                     Format (Context, To_Path, Inter_Map, Paragraph_Buffer));
                  Paragraph_Buffer := Null_Unbounded_String;
                  Append (HTML_Buffer, "</li>" & LF);
               end if;
               while not Is_Empty (List_Stack) loop
                  Append_List_Closed;
                  Delete_Last (List_Stack);
                  if not Is_Empty (List_Stack) then
                     Append_List_Indent;
                     Append (HTML_Buffer, "</li>" & LF);
                  end if;
               end loop;
            when Table_Block =>
               Append (HTML_Buffer, "</table>" & LF);
            when Nowiki_Block =>
               Append (HTML_Buffer, "</pre>" & LF);
            when Indented_Paragraph_Block =>
               Flush_Paragraph;
               Append_Indent (Indent_Level + 1);
               Append (HTML_Buffer, "</p>" & LF);
               for I in reverse 1 .. Indent_Level loop
                  Append_Indent (I);
                  Append (HTML_Buffer, "</div>" & LF);
               end loop;
               Indent_Level := 0;
            when Description_List_Block =>
               Flush_Description_List;
               Append (HTML_Buffer, "</dl>" & LF);
         end case;

         Mode := No_Block;
      end Close_Block;

      ------------------
      -- Change_Block --
      ------------------

      procedure Change_Block (New_Mode : Block_Mode) is
      begin
         Close_Block;
         Open_Block (New_Mode);
      end Change_Block;

      -------------------------
      -- Append_To_Paragraph --
      -------------------------

      procedure Append_To_Paragraph (Line : Unbounded_String) is
      begin
         if Length (Paragraph_Buffer) > 0 then
            Append (Paragraph_Buffer, LF);
         end if;
         Append (Paragraph_Buffer, Line);
      end Append_To_Paragraph;

      ---------------------------
      -- Check_Horizontal_Rule --
      ---------------------------

      function Check_Horizontal_Rule (Line : Unbounded_String) return Boolean
      is
         I : Natural := 1;
      begin
         loop
            if I > Length (Line) then
               return False;
            end if;
            exit when not Is_Whitespace (Element (Line, I));
            I := I + 1;
         end loop;
         if I + 3 > Length (Line) then
            return False;
         end if;
         if Slice (Line, I, I + 3) /= "----" then
            return False;
         end if;
         I := I + 4;
         loop
            exit when I > Length (Line);
            if not Is_Whitespace (Element (Line, I)) then
               return False;
            end if;
            I := I + 1;
         end loop;

         Change_Block (No_Block);
         Append (HTML_Buffer, "<hr />" & LF);
         return True;
      end Check_Horizontal_Rule;

      -------------------
      -- Check_Heading --
      -------------------

      function Check_Heading (Line : Unbounded_String) return Boolean is
         Level   : Natural;
         Heading : Unbounded_String;
      begin
         Parse_Heading
           (Line, Level, Heading,
            Context.Conversion.Tilde_Escape_In_Headings_Enabled);
         if Level < 1 then
            return False;
         end if;
         Change_Block (No_Block);
         declare
            Image : String := Natural'Image (Level);
            L     : String := Image (2 .. Image'Last);
         begin
            Append (HTML_Buffer, LF);
            Append (HTML_Buffer, "<h" & L & ">");
            if Context.Conversion.Heading_Anchors_Enabled then
               Append (HTML_Buffer, "<a id=""");
               Append (HTML_Buffer, HTML_Attribute_Escape (Heading));
               Append (HTML_Buffer, """>" & LF);
            end if;
            Append (HTML_Buffer, HTML_Content_Escape (Heading));
            if Context.Conversion.Heading_Anchors_Enabled then
               Append (HTML_Buffer, LF);
               Append (HTML_Buffer, "</a>");
            end if;
            Append (HTML_Buffer, "</h" & L & ">" & LF);
         end;
         if Context.Title_Level < 1 or else Level < Context.Title_Level then
            Context.Title_Level := Level;
            Context.Title       := Heading;
         end if;
         return True;
      end Check_Heading;

      -------------------
      -- Add_List_Item --
      -------------------

      procedure Add_List_Item (Kind : List_Kind; Level : Positive;
         Line                       : Unbounded_String)
      is
      begin
         if Mode /= List_Block then
            Change_Block (List_Block);
         end if;
         if Length (Paragraph_Buffer) > 0 then
            Append
              (HTML_Buffer,
               Format (Context, To_Path, Inter_Map, Paragraph_Buffer));
            Paragraph_Buffer := Null_Unbounded_String;
            if Level <= Natural (Length (List_Stack)) then
               Append (HTML_Buffer, "</li>" & LF);
            else
               Append (HTML_Buffer, LF);
            end if;
         end if;
         if Level = Natural (Length (List_Stack))
           and then Kind /= Last_Element (List_Stack) then
            Append_List_Closed;
            Append_List_Open (Kind);
            Replace_Element (List_Stack, Positive (Length (List_Stack)), Kind);
         end if;
         while Level < Natural (Length (List_Stack)) loop
            Append_List_Closed;
            Delete_Last (List_Stack);
            if not Is_Empty (List_Stack) then
               Append_List_Indent;
               Append (HTML_Buffer, "</li>" & LF);
            end if;
         end loop;
         while Level > Natural (Length (List_Stack)) loop
            Append (List_Stack, Kind);
            Append_List_Open (Kind);
            if Level > Natural (Length (List_Stack)) then
               Append_List_Indent;
               Append (HTML_Buffer, "<li>" & LF);
            end if;
         end loop;
         Append_List_Indent;
         Append (HTML_Buffer, "<li>");
         Append_To_Paragraph (Line);
      end Add_List_Item;

      ----------------
      -- Parse_List --
      ----------------

      procedure Parse_List (Line :     Unbounded_String; Kind : out List_Kind;
         Level                   : out Natural; Item : out Unbounded_String)
      is
         I    : Natural := 1;
         Mark : Character;
         L    : Natural := 0;
         J    : Natural := Length (Line);
      begin
         Kind := Unordered_List; -- dummy

         loop
            if I > Length (Line) then
               Level := 0;
               return;
            end if;
            exit when not Is_Whitespace (Element (Line, I));
            I := I + 1;
         end loop;

         Mark := Element (Line, I);
         if Mark = '*' then
            Kind := Unordered_List;
         elsif Mark = '#' then
            Kind := Ordered_List;
         else
            Level := 0;
            return;
         end if;

         loop
            L := L + 1;
            I := I + 1;
            if I > Length (Line) then
               Level := 0;
               return;
            end if;
            exit when Element (Line, I) /= Mark;
         end loop;

         loop
            exit when not Is_Whitespace (Element (Line, I));
            I := I + 1;
            if I > Length (Line) then
               Level := 0;
               return;
            end if;
         end loop;

         loop
            if J < I then
               Level := 0;
               return;
            end if;
            exit when not Is_Whitespace (Element (Line, J));
            J := J - 1;
         end loop;

         Level := L;
         Item  := Unbounded_Slice (Line, I, J);
      end Parse_List;

      ----------------
      -- Check_List --
      ----------------

      function Check_List (Line : Unbounded_String) return Boolean is
         Kind  : List_Kind;
         Level : Natural;
         Item  : Unbounded_String;
      begin
         Parse_List (Line, Kind, Level, Item);
         if Level < 1 then
            return False;
         end if;
         if Mode /= List_Block and then Level > 1 then
            return False;
         end if;
         Add_List_Item (Kind, Level, Item);
         return True;
      end Check_List;

      -----------------
      -- Check_Table --
      -----------------

      function Check_Table (Line : Unbounded_String) return Boolean is
         I : Natural := 1;
      begin
         loop
            if I > Length (Line) then
               return False;
            end if;
            exit when not Is_Whitespace (Element (Line, I));
            I := I + 1;
         end loop;
         if Element (Line, I) /= '|' then
            return False;
         end if;
         if Mode /= Table_Block then
            Change_Block (Table_Block);
         end if;
         declare
            Found : Boolean := False;
         begin
            loop
               declare
                  Heading : Boolean := False;
                  J       : Natural := I + 1;
               begin
                  if J <= Length (Line) and then Element (Line, J) = '=' then
                     Heading := True;
                     J       := J + 1;
                  end if;
                  loop
                     if J > Length (Line) then
                        if Found then
                           Append (HTML_Buffer, "</tr>" & LF);
                           return True;
                        else
                           return False;
                        end if;
                     end if;
                     exit when not Is_Whitespace (Element (Line, J));
                     J := J + 1;
                  end loop;
                  if not Found then
                     Found := True;
                     Append (HTML_Buffer, "<tr>" & LF);
                  end if;
                  if Heading then
                     Append (HTML_Buffer, "<th>");
                  else
                     Append (HTML_Buffer, "<td>");
                  end if;
                  declare
                     Bar_Index         : Natural;
                     Closed_Mark_Index : Natural;
                     Formatted         : Unbounded_String :=
                       Find_And_Format
                         (Context, To_Path, Inter_Map, Line, J, Length (Line),
                          True, '|', Bar_Index, "", Closed_Mark_Index);
                  begin
                     Append (HTML_Buffer, Trim_Whitespaces (Formatted));
                     if Heading then
                        Append (HTML_Buffer, "</th>" & LF);
                     else
                        Append (HTML_Buffer, "</td>" & LF);
                     end if;
                     if Bar_Index > 0 then
                        I := Bar_Index;
                     else
                        I := Length (Line) + 1;
                     end if;
                  end;
               end;
            end loop;
         end;
      end Check_Table;

      ----------------------------------
      -- Check_Nowiki_Indented_Closed --
      ----------------------------------

      function Check_Nowiki_Indented_Closed
        (Line : Unbounded_String) return Boolean
      is

         ------------------------
         -- Is_Indedted_Closed --
         ------------------------

         function Is_Indented_Closed return Boolean is
         begin
            if Length (Line) < 4 then
               return False;
            end if;
            for I in 1 .. Length (Line) - 3 loop
               if not Is_Whitespace (Element (Line, I)) then
                  return False;
               end if;
            end loop;
            return Slice (Line, Length (Line) - 2, Length (Line)) = "}}}";
         end Is_Indented_Closed;
      begin
         if not Is_Indented_Closed then
            return False;
         end if;
         Append
           (HTML_Buffer,
            HTML_Content_Escape (Unbounded_Slice (Line, 2, Length (Line))));
         Append (HTML_Buffer, LF);
         return True;
      end Check_Nowiki_Indented_Closed;

      ------------------------------------
      -- Append_Indented_Paragraph_Open --
      ------------------------------------

      procedure Append_Indented_Paragraph_Open is
      begin
         Append
           (HTML_Buffer, "<div " & Context.Conversion.Indent_Style & ">" & LF);
      end Append_Indented_Paragraph_Open;

      ------------------------------
      -- Check_Indented_Paragraph --
      ------------------------------

      function Check_Indented_Paragraph
        (Line : Unbounded_String) return Boolean
      is
         Level : Natural  := 0;
         I     : Positive := 1;
      begin
         loop
            exit when I > Length (Line);
            exit when Element (Line, I) /= ':';
            Level := Level + 1;
            I     := I + 1;
         end loop;
         if Level < 1 then
            loop
               exit when I > Length (Line);
               exit when not Is_Whitespace (Element (Line, I));
               I := I + 1;
            end loop;
            loop
               exit when I > Length (Line);
               exit when Element (Line, I) /= '>';
               Level := Level + 1;
               I     := I + 1;
            end loop;
            if Level > 0 then
               loop
                  exit when I > Length (Line);
                  exit when not Is_Whitespace (Element (Line, I));
                  I := I + 1;
               end loop;
            end if;
         end if;
         if Level < 1 then
            if Mode = Indented_Paragraph_Block then
               Append_To_Paragraph (Line);
               return True;
            else
               return False;
            end if;
         end if;
         if Mode = Indented_Paragraph_Block then
            Flush_Paragraph;
            Append_Indent (Indent_Level + 1);
            Append (HTML_Buffer, "</p>" & LF);
            declare
               L : Natural := Indent_Level;
            begin
               while L > Level loop
                  Append_Indent (L);
                  Append (HTML_Buffer, "</div>" & LF);
                  L := L - 1;
               end loop;
               while L < Level loop
                  L := L + 1;
                  Append_Indent (L);
                  Append_Indented_Paragraph_Open;
               end loop;
            end;
            Indent_Level := Level;
            Append_Indent (Level + 1);
            Append (HTML_Buffer, "<p>" & LF);
         else
            Change_Block (Indented_Paragraph_Block);
            for I in 1 .. Level loop
               Append_Indent (I);
               Append_Indented_Paragraph_Open;
            end loop;
            Indent_Level := Level;
            Append_Indent (Level + 1);
            Append (HTML_Buffer, "<p>" & LF);
         end if;
         Append_To_Paragraph (Unbounded_Slice (Line, I, Length (Line)));
         return True;
      end Check_Indented_Paragraph;

      ----------------------------
      -- Check_Description_List --
      ----------------------------

      function Check_Description_List (Line : Unbounded_String) return Boolean
      is
      begin
         if Mode = Description_List_Block then
            if Length (Line) > 0 and then Element (Line, 1) = ';' then
               Flush_Description_List;
               if Length (Line) > 1 then
                  Append_To_Paragraph
                    (Unbounded_Slice (Line, 2, Length (Line)));
               end if;
            else
               Append_To_Paragraph (Line);
            end if;
            return True;
         else
            if Length (Line) > 0 and then Element (Line, 1) = ';' then
               Change_Block (Description_List_Block);
               if Length (Line) > 1 then
                  Append_To_Paragraph
                    (Unbounded_Slice (Line, 2, Length (Line)));
               end if;
               return True;
            else
               return False;
            end if;
         end if;
      end Check_Description_List;

   begin
      loop
         begin
            Line := Get_Line (Input_Stream);
         exception
            when Ada.IO_Exceptions.End_Error =>
               exit;
         end;

         if Mode = No_Block then
            if Length (Line) < 1 then
               goto Continue;
            end if;
            if Line = "{{{" then
               Change_Block (Nowiki_Block);
               goto Continue;
            end if;
            if Check_Horizontal_Rule (Line) then
               goto Continue;
            end if;
            if Check_Heading (Line) then
               goto Continue;
            end if;
            if Check_List (Line) then
               goto Continue;
            end if;
            if Check_Table (Line) then
               goto Continue;
            end if;
            if Context.Conversion.Additions_Enabled then
               -- Description list must precede indented paragraph.
               if Check_Description_List (Line) then
                  goto Continue;
               end if;
               if Check_Indented_Paragraph (Line) then
                  goto Continue;
               end if;
            end if;
            Change_Block (Paragraph_Block);
         end if;

         case Mode is

            when No_Block =>
               null;

            when Paragraph_Block =>
               if Length (Line) < 1 then
                  Change_Block (No_Block);
                  goto Continue;
               end if;
               if Line = "{{{" then
                  Change_Block (Nowiki_Block);
                  goto Continue;
               end if;
               if Check_Horizontal_Rule (Line) then
                  goto Continue;
               end if;
               if Check_Heading (Line) then
                  goto Continue;
               end if;
               if Check_List (Line) then
                  goto Continue;
               end if;
               if Check_Table (Line) then
                  goto Continue;
               end if;
               if Context.Conversion.Additions_Enabled then
                  -- Description list must precede indented paragraph.
                  if Check_Description_List (Line) then
                     goto Continue;
                  end if;
                  if Check_Indented_Paragraph (Line) then
                     goto Continue;
                  end if;
               end if;
               Append_To_Paragraph (Line);

            when List_Block =>
               if Length (Line) < 1 then
                  Change_Block (No_Block);
                  goto Continue;
               end if;
               if Line = "{{{" then
                  Change_Block (Nowiki_Block);
                  goto Continue;
               end if;
               if Check_Horizontal_Rule (Line) then
                  goto Continue;
               end if;
               if Check_Heading (Line) then
                  goto Continue;
               end if;
               if Check_List (Line) then
                  goto Continue;
               end if;
               if Check_Table (Line) then
                  goto Continue;
               end if;
               if Context.Conversion.Additions_Enabled then
                  -- Description list must precede indented paragraph.
                  if Check_Description_List (Line) then
                     goto Continue;
                  end if;
                  if Check_Indented_Paragraph (Line) then
                     goto Continue;
                  end if;
               end if;
               Append_To_Paragraph (Line);

            when Table_Block =>
               if Length (Line) < 1 then
                  Change_Block (No_Block);
                  goto Continue;
               end if;
               if Line = "{{{" then
                  Change_Block (Nowiki_Block);
                  goto Continue;
               end if;
               if Check_Horizontal_Rule (Line) then
                  goto Continue;
               end if;
               if Check_Heading (Line) then
                  goto Continue;
               end if;
               if Check_List (Line) then
                  goto Continue;
               end if;
               if Check_Table (Line) then
                  goto Continue;
               end if;
               if Context.Conversion.Additions_Enabled then
                  -- Description list must precede indented paragraph.
                  if Check_Description_List (Line) then
                     goto Continue;
                  end if;
                  if Check_Indented_Paragraph (Line) then
                     goto Continue;
                  end if;
               end if;
               Change_Block (Paragraph_Block);
               Append_To_Paragraph (Line);

            when Nowiki_Block =>
               if Line = "}}}" then
                  Change_Block (No_Block);
                  goto Continue;
               end if;
               if Check_Nowiki_Indented_Closed (Line) then
                  goto Continue;
               end if;
               Append (HTML_Buffer, HTML_Content_Escape (Line));
               Append (HTML_Buffer, LF);

            when Indented_Paragraph_Block =>
               if Length (Line) < 1 then
                  Change_Block (No_Block);
                  goto Continue;
               end if;
               if Line = "{{{" then
                  Change_Block (Nowiki_Block);
                  goto Continue;
               end if;
               if Check_Horizontal_Rule (Line) then
                  goto Continue;
               end if;
               if Check_Heading (Line) then
                  goto Continue;
               end if;
               if Check_List (Line) then
                  goto Continue;
               end if;
               if Check_Table (Line) then
                  goto Continue;
               end if;
               -- Description list must precede indented paragraph.
               if Check_Description_List (Line) then
                  goto Continue;
               end if;
               if Check_Indented_Paragraph (Line) then
                  goto Continue;
               end if;
               Append_To_Paragraph (Line);

            when Description_List_Block =>
               if Length (Line) < 1 then
                  Change_Block (No_Block);
                  goto Continue;
               end if;
               if Line = "{{{" then
                  Change_Block (Nowiki_Block);
                  goto Continue;
               end if;
               if Check_Horizontal_Rule (Line) then
                  goto Continue;
               end if;
               if Check_Heading (Line) then
                  goto Continue;
               end if;
               if Check_List (Line) then
                  goto Continue;
               end if;
               if Check_Table (Line) then
                  goto Continue;
               end if;
               -- Description list must precede indented paragraph.
               if Check_Description_List (Line) then
                  goto Continue;
               end if;
               if Check_Indented_Paragraph (Line) then
                  goto Continue;
               end if;
               Append_To_Paragraph (Line);

         end case;

         <<Continue>>
      end loop;

      Change_Block (No_Block);

      if HTML_Output_Stream = null then
         return;
      end if;

      declare
         Output_Stream : Buffered_Stream_Type (HTML_Output_Stream,
            Buffer_Size);
      begin
         if not Context.Conversion.Embedded then
            Put
              (Output_Stream,
               Create_HTML_Header
                 (Context.Conversion.Style_Sheet_URL, Context.Title,
                  Context.Format.MathJax_Enabled));
         end if;
         Put (Output_Stream, HTML_Buffer);
         if not Context.Conversion.Embedded then
            Put (Output_Stream, LF);
            Put (Output_Stream, Create_HTML_Footer);
         end if;
         Flush (Output_Stream);
      end;
   end Convert;

end Licana.Converters;
