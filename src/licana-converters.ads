with Ada.Streams.Stream_IO; use Ada.Streams.Stream_IO;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with Licana.Contexts;       use Licana.Contexts;
with Licana.Inter_Maps;     use Licana.Inter_Maps;

package Licana.Converters is

   procedure Convert (Context : in out Contexts.Context;
      To_Path                 :        access function
        (Name : Unbounded_String) return Unbounded_String;
      Inter_Map : Inter_Maps.Inter_Map; Text_Input_Stream : Stream_Access;
      HTML_Output_Stream : Stream_Access);

end Licana.Converters;
