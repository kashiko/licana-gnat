with Ada.Directories; use Ada.Directories;

package body Licana.Directories is

   procedure Apply_To_Directories (Directory : String;
      Proc : access procedure (Directory : String))
   is
      Search : Search_Type;
   begin
      Start_Search
        (Search, Directory, "",
         (Ada.Directories.Directory => True, others => False));
      while More_Entries (Search) loop
         declare
            Directory_Entry : Directory_Entry_Type;
         begin
            Get_Next_Entry (Search, Directory_Entry);
            declare
               Name : constant String := Simple_Name (Directory_Entry);
            begin
               if Name = "." or else Name = ".." then
                  goto Continue;
               end if;
            end;
            Proc (Full_Name (Directory_Entry));
         end;
         <<Continue>>
      end loop;
      End_Search (Search);
   end Apply_To_Directories;

   procedure Apply_To_Directories_Recursively (Directory : String;
      Proc : access procedure (Directory : String))
   is
      Search : Search_Type;
   begin
      Start_Search
        (Search, Directory, "",
         (Ada.Directories.Directory => True, others => False));
      while More_Entries (Search) loop
         declare
            Directory_Entry : Directory_Entry_Type;
         begin
            Get_Next_Entry (Search, Directory_Entry);
            declare
               Name : constant String := Simple_Name (Directory_Entry);
            begin
               if Name = "." or else Name = ".." then
                  goto Continue;
               end if;
            end;
            if Active (Debug_Logger) then
               Trace (Debug_Logger, "Enter " & Full_Name (Directory_Entry));
            end if;
            Apply_To_Directories_Recursively
              (Full_Name (Directory_Entry), Proc);
         end;
         <<Continue>>
      end loop;
      End_Search (Search);
      Proc (Directory);
   end Apply_To_Directories_Recursively;

end Licana.Directories;
