package Licana.Directories is

   procedure Apply_To_Directories (Directory : String;
      Proc : access procedure (Directory : String));

   procedure Apply_To_Directories_Recursively (Directory : String;
      Proc : access procedure (Directory : String));

end Licana.Directories;
