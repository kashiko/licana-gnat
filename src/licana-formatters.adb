with Ada.Characters.Latin_1; use Ada.Characters.Latin_1;
with Licana.HTML_Utils;      use Licana.HTML_Utils;
with Licana.Strings;         use Licana.Strings;
with Licana.URL_Utils;       use Licana.URL_Utils;

package body Licana.Formatters is

   -----------------------
   -- Is_Tilde_Escaping --
   -----------------------

   function Is_Tilde_Escaping (S : Unbounded_String; Index : Positive;
      Last                       : Natural) return Boolean
   is
   begin
      if Element (S, Index) /= '~' then
         return False;
      end if;
      if Index + 1 > Last then
         return False;
      end if;
      if Is_Whitespace_Or_Newline (Element (S, Index + 1)) then
         return False;
      end if;
      return True;
   end Is_Tilde_Escaping;

   --------------------
   -- Tilde_Unescape --
   --------------------

   function Tilde_Unescape (S : Unbounded_String; First : Positive;
      Last                    : Natural) return Unbounded_String
   is
      I : Natural := Find (S, First, Last, '~');
   begin
      if I < 1 then
         if First = 1 and then Last = Length (S) then
            return S;
         end if;
         return Unbounded_Slice (S, First, Last);
      end if;
      return R : Unbounded_String do
         Append (R, Unbounded_Slice (S, First, I - 1));
         loop
            if Is_Tilde_Escaping (S, I, Last) then
               I := I + 1;
            end if;
            Append (R, Element (S, I));
            I := I + 1;
            exit when I > Last;
         end loop;
      end return;
   end Tilde_Unescape;

   --------------------
   -- Find_Last_Mark --
   --------------------

   function Find_Last_Mark (Context : in out Contexts.Context;
      To_Path                       :        access function
        (Name : Unbounded_String) return Unbounded_String;
      Inter_Map : Inter_Maps.Inter_Map; S : Unbounded_String; First : Positive;
      Last      :     Natural; Mark : String; Open_End : Boolean;
      Formatted : out Unbounded_String; Escape : Boolean) return Natural
   is
      Bar_Index         : Natural;
      Closed_Mark_Index : Natural;
   begin
      Formatted :=
        Find_And_Format
          (Context, To_Path, Inter_Map, S, First, Last, Open_End, NUL,
           Bar_Index, Mark, Closed_Mark_Index, Escape);
      if Closed_Mark_Index < 1 then
         if Open_End then
            return Last + 1;
         else
            return 0;
         end if;
      end if;
      loop
         declare
            Temp_First : constant Positive := Closed_Mark_Index + Mark'Length;
            Temp_Closed_Mark_Index : Natural;
            Temp_Formatted         : constant Unbounded_String :=
              Find_And_Format
                (Context, To_Path, Inter_Map, S, Temp_First, Last, Open_End,
                 NUL, Bar_Index, Mark, Temp_Closed_Mark_Index, Escape);
         begin
            if Temp_Closed_Mark_Index < 1 then
               return Closed_Mark_Index;
            end if;
            Append (Formatted, Mark);
            Append (Formatted, Temp_Formatted);
            Closed_Mark_Index := Temp_Closed_Mark_Index;
         end;
      end loop;
   end Find_Last_Mark;

   -------------------------------
   -- Find_Closed_Mark_With_Bar --
   -------------------------------

   function Find_Closed_Mark_With_Bar (Context : in out Contexts.Context;
      To_Path                                  :        access function
        (Name : Unbounded_String) return Unbounded_String;
      Inter_Map : Inter_Maps.Inter_Map; S : Unbounded_String; First : Positive;
      Last      :     Natural; Mark : String; Formatted : out Unbounded_String;
      Bar_Index : out Natural; Escape : Boolean := False) return Natural
   is
      Closed_Mark_Index : Natural := 0;
      I                 : Natural := First;
   begin
      Bar_Index := 0;
      loop
         exit when I > Last;
         if Escape and then Is_Tilde_Escaping (S, I, Last) then
            I := I + 1;
         elsif Element (S, I) = '|' then
            Bar_Index := I;
            exit;
         elsif Starts_With (S, I, Last, Mark) then
            Closed_Mark_Index := I;
            exit;
         end if;
         I := I + 1;
      end loop;
      if Bar_Index > 0 then
         declare
            I_Bar_Index : Natural;
         begin
            Formatted :=
              Find_And_Format
                (Context, To_Path, Inter_Map, S, Bar_Index + 1, Last, False,
                 NUL, I_Bar_Index, Mark, Closed_Mark_Index, True, True);
         end;
      end if;
      return Closed_Mark_Index;
   end Find_Closed_Mark_With_Bar;

   -----------------------------------------
   -- Find_Closed_Mark_With_Bar_For_Image --
   -----------------------------------------

   function Find_Closed_Mark_With_Bar_For_Image (S :     Unbounded_String;
      First :     Positive; Last : Natural; Mark : String;
      Bar_Index : out Natural) return Natural
   is
      E : Boolean := False;
      I : Natural := First;
   begin
      Bar_Index := 0;
      loop
         if I + Mark'Length - 1 > Last then
            return 0;
         end if;
         if not E and then Element (S, I) = '|' then
            E := True;
         end if;
         if E and then Is_Tilde_Escaping (S, I, Last) then
            I := I + 1;
         elsif Starts_With (S, I, Last, Mark) then
            return I;
         elsif Bar_Index < 1 and then Element (S, I) = '|' then
            Bar_Index := I;
         end if;
         I := I + 1;
      end loop;
   end Find_Closed_Mark_With_Bar_For_Image;

   -------------------------------------
   -- Find_Closed_Mark_Without_Format --
   -------------------------------------

   function Find_Closed_Mark_Without_Format (S : Unbounded_String;
      First : Positive; Last : Natural; Mark : String; Open_End : Boolean;
      Escape                                   : Boolean) return Natural
   is
      I : Positive := First;
   begin
      loop
         if I > Last then
            if Open_End then
               return Last;
            else
               return 0;
            end if;
         end if;
         if Escape and then Is_Tilde_Escaping (S, I, Last) then
            I := I + 1;
         elsif Starts_With (S, I, Last, Mark) then
            return I - 1;
         end if;
         I := I + 1;
      end loop;
   end Find_Closed_Mark_Without_Format;

   ---------------------
   -- Find_And_Format --
   ---------------------

   function Find_And_Format (Context : in out Contexts.Context;
      To_Path                        :        access function
        (Name : Unbounded_String) return Unbounded_String;
      Inter_Map : Inter_Maps.Inter_Map; S : Unbounded_String; First : Positive;
      Last :     Natural; Open_End : Boolean; Bar_Character : Character;
      Bar_Index         : out Natural; Closed_Mark_String : String;
      Closed_Mark_Index : out Natural; Escape_Flag : Boolean := True;
      Bracket_Mode      :     Boolean := False) return Unbounded_String
   is
      I      : Positive := First;
      Buffer : Unbounded_String;

      --------------------
      -- Apply_Emphasis --
      --------------------

      function Apply_Emphasis (Open_Mark : String; Closed_Mark : String;
         Open_Tag                        : String; Closed_Tag : String;
         Open_End                        : Boolean) return Boolean
      is

         -----------------------------------
         -- Find_Closed_Mark_For_Emphasis --
         -----------------------------------

         function Find_Closed_Mark_For_Emphasis
           (Context  : in out Contexts.Context; S : Unbounded_String;
            First    :        Positive; Last : Natural; Mark : String;
            Open_End :        Boolean; Formatted : out Unbounded_String;
            Escape   :        Boolean) return Natural
         is
            Closed_Mark_Index : Natural;
         begin
            Formatted :=
              Find_And_Format
                (Context, To_Path, Inter_Map, S, First, Last, Open_End,
                 Bar_Character, Bar_Index, Mark, Closed_Mark_Index, Escape);
            if Closed_Mark_Index < 1 then
               if Open_End then
                  return Last + 1;
               else
                  return 0;
               end if;
            else
               return Closed_Mark_Index;
            end if;
         end Find_Closed_Mark_For_Emphasis;

      begin
         if not Starts_With (S, I, Last, Open_Mark) then
            return False;
         end if;
         declare
            Formatted  : Unbounded_String;
            Mark_Index : Natural :=
              Find_Closed_Mark_For_Emphasis
                (Context, S, I + Open_Mark'Length, Last, Closed_Mark, Open_End,
                 Formatted, True);
         begin
            if Context.Format.Null_Contents_Enabled then
               if Mark_Index < 1 then
                  return False;
               end if;
            else
               if Mark_Index <= I + Open_Mark'Length then
                  return False;
               end if;
            end if;
            Append (Buffer, Open_Tag);
            declare
               Inner_Open_End : Boolean := Open_End and then Mark_Index > Last;
            begin
               Append (Buffer, Formatted);
            end;
            Append (Buffer, Closed_Tag);
            I := Natural'Min (Mark_Index + Closed_Mark'Length - 1, Last);
            return True;
         end;
      end Apply_Emphasis;

      -------------------
      -- Apply_Bracket --
      -------------------

      function Apply_Bracket return Boolean is
      begin
         if not Starts_With (S, I, Last, "[[") then
            return False;
         end if;
         declare
            P : constant Natural :=
              URL_Schemes_Colon_Index
                (S, I + 2, Last, Context.Format.URL_Schemes);
            URL        : constant Boolean := P >= I + 2;
            Formatted  : Unbounded_String;
            Bar        : Natural;
            Mark_Index : constant Natural :=
              Find_Closed_Mark_With_Bar
                (Context, To_Path, Inter_Map, S, I + 2, Last, "]]", Formatted,
                 Bar, not URL);
         begin
            if Context.Format.Null_Contents_Enabled then
               if Mark_Index < 1 then
                  return False;
               end if;
            else
               if Mark_Index <= I + 2 then
                  return False;
               end if;
            end if;
            declare
               Address : Unbounded_String;
            begin
               if Bar > 0 then
                  if not Context.Format.Null_Contents_Enabled then
                     if Bar <= I + 2 then
                        return False;
                     end if;
                     if Bar >= Mark_Index - 1 then
                        return False;
                     end if;
                  end if;
                  if URL then
                     Address := Unbounded_Slice (S, I + 2, Bar - 1);
                  else
                     declare
                        Segment : constant Unbounded_String :=
                          Tilde_Unescape (S, I + 2, Bar - 1);
                        Inter_Path : constant Unbounded_String :=
                          Get_Path (Inter_Map, Segment);
                     begin
                        if Length (Inter_Path) < 1 then
                           declare
                              Path : constant Unbounded_String :=
                                To_Path (Segment);
                           begin
                              if Length (Path) < 1 then
                                 Address := Segment;
                              else
                                 Address := Path;
                              end if;
                           end;
                        else
                           Address := Inter_Path;
                        end if;
                     end;
                  end if;
                  Append (Buffer, "<a href=""");
                  Append
                    (Buffer,
                     HTML_Attribute_Escape
                       (URL_Encode_Except_Percent (Address)));
                  Append (Buffer, """>");
                  Append (Buffer, Formatted);
                  Append (Buffer, "</a>");
               else
                  if Context.Format.Additions_Enabled then
                     declare
                        A_Formatted  : Unbounded_String;
                        A_Mark_Index : constant Natural :=
                          Find_Last_Mark
                            (Context, To_Path, Inter_Map, S, I + 2,
                             Mark_Index - 1, "->", False, A_Formatted, True);
                     begin
                        if A_Mark_Index > 0 then
                           Address :=
                             Trim_Whitespaces
                               (Unbounded_Slice
                                  (S, A_Mark_Index + 2, Mark_Index - 1));
                           declare
                              P2 : constant Natural :=
                                URL_Schemes_Colon_Index
                                  (Address, 1, Length (Address),
                                   Context.Format.URL_Schemes);
                              URL2 : constant Boolean := P2 >= 1;
                           begin
                              Append (Buffer, "<a href=""");
                              if URL2 then
                                 Append
                                   (Buffer,
                                    HTML_Attribute_Escape
                                      (URL_Encode_Except_Percent (Address)));
                              else
                                 declare
                                    Segment : constant Unbounded_String :=
                                      Tilde_Unescape
                                        (Address, 1, Length (Address));
                                    Addr_URL   : Unbounded_String;
                                    Inter_Path : constant Unbounded_String :=
                                      Get_Path (Inter_Map, Segment);
                                 begin
                                    if Length (Inter_Path) < 1 then
                                       declare
                                          Path : constant Unbounded_String :=
                                            To_Path (Segment);
                                       begin
                                          if Length (Path) < 1 then
                                             Addr_URL := Segment;
                                          else
                                             Addr_URL := Path;
                                          end if;
                                       end;
                                    else
                                       Addr_URL := Inter_Path;
                                    end if;
                                    Append
                                      (Buffer,
                                       HTML_Attribute_Escape
                                         (URL_Encode_Except_Percent
                                            (Addr_URL)));
                                 end;
                              end if;
                              Append (Buffer, """>");
                              Append (Buffer, Trim_Whitespaces (A_Formatted));
                              Append (Buffer, "</a>");
                           end;
                           goto End_Of_Subprogram;
                        end if;
                     end;
                  end if;
                  if URL then
                     Address := Unbounded_Slice (S, I + 2, Mark_Index - 1);
                  else
                     Address := Tilde_Unescape (S, I + 2, Mark_Index - 1);
                  end if;
                  if Context.Format.URL_Fragment_Prefix_Omission_Enabled
                    and then Address = "#"
                    and then not Context.Format.Null_Contents_Enabled then
                     return False;
                  end if;
                  Append (Buffer, "<a href=""");
                  if URL then
                     Append
                       (Buffer,
                        HTML_Attribute_Escape
                          (URL_Encode_Except_Percent (Address)));
                  else
                     declare
                        Addr_URL   : Unbounded_String;
                        Inter_Path : constant Unbounded_String :=
                          Get_Path (Inter_Map, Address);
                     begin
                        if Length (Inter_Path) < 1 then
                           declare
                              Path : constant Unbounded_String :=
                                To_Path (Address);
                           begin
                              if Length (Path) < 1 then
                                 Addr_URL := Address;
                              else
                                 Addr_URL := Path;
                              end if;
                           end;
                        else
                           Addr_URL := Inter_Path;
                        end if;
                        Append
                          (Buffer,
                           HTML_Attribute_Escape
                             (URL_Encode_Except_Percent (Addr_URL)));
                     end;
                  end if;
                  Append (Buffer, """>");
                  if Context.Format.URL_Fragment_Prefix_Omission_Enabled
                    and then Element (Address, 1) = '#' then
                     Append
                       (Buffer,
                        HTML_Content_Escape
                          (Unbounded_Slice (Address, 2, Length (Address))));
                  else
                     Append (Buffer, HTML_Content_Escape (Address));
                  end if;
                  Append (Buffer, "</a>");
               end if;
            end;
            <<End_Of_Subprogram>>
            I := Mark_Index + 2 - 1;
            return True;
         end;
      end Apply_Bracket;

      ---------------
      -- Apply_URL --
      ---------------

      function Apply_URL (Escape : Boolean := False) return Boolean is
         J : Natural :=
           URL_Schemes_Colon_Index (S, I, Last, Context.Format.URL_Schemes);
         Punctuations : constant String := ",.?!:;""'";
      begin
         if J = 0 then
            return False;
         end if;
         declare
            Incremented : Boolean := False;
         begin
            loop
               exit when J + 1 > Last;
               exit when not Is_URL_Character_Or_Percent_Or_Multibyte
                   (Element (S, J + 1));
               exit when Element (S, J + 1) = Bar_Character;
               exit when Bracket_Mode
                 and then Starts_With (S, J + 1, Last, Closed_Mark_String);
               J           := J + 1;
               Incremented := True;
            end loop;
            if Incremented then
               declare
                  C : constant Character := Element (S, J);
               begin
                  for P of Punctuations loop
                     if C = P then
                        J := J - 1;
                        exit;
                     end if;
                  end loop;
               end;
            end if;
         end;
         declare
            URL : Unbounded_String := Unbounded_Slice (S, I, J);
         begin
            if Escape then
               Append (Buffer, HTML_Content_Escape (URL));
            else
               Append (Buffer, "<a href=""");
               Append
                 (Buffer,
                  HTML_Attribute_Escape (URL_Encode_Except_Percent (URL)));
               Append (Buffer, """>");
               Append (Buffer, HTML_Content_Escape (URL));
               Append (Buffer, "</a>");
            end if;
         end;
         I := J;
         return True;
      end Apply_URL;

      ----------------------
      -- Apply_Line_Break --
      ----------------------

      function Apply_Line_Break return Boolean is
      begin
         if not Starts_With (S, I, Last, "\\") then
            return False;
         end if;
         Append (Buffer, "<br />");
         I := I + 1;
         return True;
      end Apply_Line_Break;

      -----------------
      -- Apply_Image --
      -----------------

      function Apply_Image return Boolean is
      begin
         if not Starts_With (S, I, Last, "{{") then
            return False;
         end if;
         declare
            Bar        : Natural;
            Mark_Index : Natural :=
              Find_Closed_Mark_With_Bar_For_Image (S, I + 2, Last, "}}", Bar);
         begin
            if Context.Format.Null_Contents_Enabled then
               if Mark_Index < 1 then
                  return False;
               end if;
            else
               if Mark_Index <= I + 2 then
                  return False;
               end if;
               if Bar <= I + 2 then
                  return False;
               end if;
               if Bar >= Mark_Index - 1 then
                  return False;
               end if;
            end if;
            declare
               Address : Unbounded_String;
               Label   : Unbounded_String;
            begin
               if I + 2 <= Bar - 1 then
                  Address := Unbounded_Slice (S, I + 2, Bar - 1);
               end if;
               if Bar + 1 <= Mark_Index - 1 then
                  Label := Tilde_Unescape (S, Bar + 1, Mark_Index - 1);
               end if;
               Append (Buffer, "<img src=""");
               Append (Buffer, HTML_Attribute_Escape (Address));
               Append (Buffer, """ alt=""");
               Append (Buffer, HTML_Attribute_Escape (Label));
               Append (Buffer, """ />");
            end;
            I := Mark_Index + 2 - 1;
            return True;
         end;
      end Apply_Image;

      ------------------
      -- Apply_Nowiki --
      ------------------

      function Apply_Nowiki return Boolean is
      begin
         if not Starts_With (S, I, Last, "{{{") then
            return False;
         end if;
         declare
            I_Last : Natural := I + 3;
            Extra  : Natural := 0;
         begin
            loop
               if I_Last + 2 > Last then
                  I_Last := 0;
                  exit;
               end if;
               if Starts_With (S, I_Last, Last, "}}}") then
                  I_Last := I_Last - 1;
                  exit;
               end if;
               I_Last := I_Last + 1;
            end loop;
            if I_Last < I + 3 then
               return False;
            end if;
            loop
               exit when I_Last + 4 + Extra > Last;
               exit when Element (S, I_Last + 4 + Extra) /= '}';
               Extra := Extra + 1;
            end loop;
            Append (Buffer, "<code>");
            Append
              (Buffer,
               HTML_Content_Escape
                 (Unbounded_Slice (S, I + 3, I_Last + Extra)));
            Append (Buffer, "</code>");
            I := I_Last + 3 + Extra;
            return True;
         end;
      end Apply_Nowiki;

      -----------------------
      -- Apply_Placeholder --
      -----------------------

      function Apply_Placeholder return Boolean is
      begin
         if not Starts_With (S, I, Last, "<<<") then
            return False;
         end if;
         declare
            I_Last : Natural :=
              Find_Closed_Mark_Without_Format
                (S, I + 3, Last, ">>>", False, False);
         begin
            if I_Last < I + 3 then
               return False;
            end if;
            declare
               Content : Unbounded_String :=
                 Unbounded_Slice (S, I + 3, I_Last);
            begin
               if Context.Format.Null_Contents_Enabled then
                  Append (Buffer, "<!-- Placeholder: ");
                  Append (Buffer, HTML_Content_Escape (Content));
                  Append (Buffer, " -->");
               else
                  Append (Buffer, HTML_Content_Escape ("<<<"));
                  Append (Buffer, HTML_Content_Escape (Content));
                  Append (Buffer, HTML_Content_Escape (">>>"));
               end if;
            end;
            I := I_Last + 3;
            return True;
         end;
      end Apply_Placeholder;

      ---------------------
      -- Apply_Extension --
      ---------------------

      function Apply_Extension return Boolean is
      begin
         if not Starts_With (S, I, Last, "<<") then
            return False;
         end if;
         declare
            I_Last : Natural :=
              Find_Closed_Mark_Without_Format
                (S, I + 2, Last, ">>", False, False);
         begin
            if I_Last < I + 2 then
               return False;
            end if;
            declare
               Content : Unbounded_String :=
                 Unbounded_Slice (S, I + 2, I_Last);
            begin
               if Starts_With (Content, 1, Length (Content), "title ") then
                  if Length (Content) < 7 then
                     Context.Title := Null_Unbounded_String;
                  else
                     Context.Title :=
                       Unbounded_Slice (Content, 7, Length (Content));
                  end if;
                  Context.Title_Level := 1;
               elsif Content = "enable MathJax" then
                  Context.Format.MathJax_Enabled := True;
               else
                  if Context.Format.Null_Contents_Enabled then
                     Append (Buffer, "<!-- Extension: ");
                     Append (Buffer, HTML_Content_Escape (Content));
                     Append (Buffer, " -->");
                  else
                     Append (Buffer, HTML_Content_Escape ("<<"));
                     Append (Buffer, HTML_Content_Escape (Content));
                     Append (Buffer, HTML_Content_Escape (">>"));
                  end if;
               end if;
            end;
            I := I_Last + 2;
            return True;
         end;
      end Apply_Extension;

      ---------------------
      -- Apply_Additions --
      ---------------------

      function Apply_Additions return Boolean is
      begin
         if Apply_Extension then
            return True;
         elsif Apply_Emphasis ("##", "##", "<code>", "</code>", Open_End) then
            return True;
         elsif Apply_Emphasis ("^^", "^^", "<sup>", "</sup>", Open_End) then
            return True;
         elsif Apply_Emphasis (",,", ",,", "<sub>", "</sub>", Open_End) then
            return True;
         elsif Apply_Emphasis ("__", "__", "<u>", "</u>", Open_End) then
            return True;
         else
            return False;
         end if;
      end Apply_Additions;

      -------------------------------
      -- Apply_Escape_MathJax_Mark --
      -------------------------------

      function Apply_Escape_MathJax_Mark (Mark : String) return Boolean is
      begin
         if not Starts_With (S, I, Last, Mark) then
            return False;
         end if;
         Append (Buffer, "<span class=""tex2jax_ignore"">");
         Append (Buffer, Mark);
         Append (Buffer, "</span>");
         I := I + Mark'Length - 1;
         return True;
      end Apply_Escape_MathJax_Mark;

      --------------------------
      -- Aplly_Escape_MathJax --
      --------------------------

      function Apply_Escape_MathJax return Boolean is
      begin
         if Apply_Escape_MathJax_Mark ("\[") then
            return True;
         elsif Apply_Escape_MathJax_Mark ("\]") then
            return True;
         elsif Apply_Escape_MathJax_Mark ("\(") then
            return True;
         elsif Apply_Escape_MathJax_Mark ("\)") then
            return True;
         elsif Apply_Escape_MathJax_Mark ("$$") then
            return True;
         else
            return False;
         end if;
      end Apply_Escape_MathJax;

      -------------------------
      -- Apply_Quote_MathJax --
      -------------------------

      function Apply_Quote_MathJax (Open_Mark : String;
         Closed_Mark                          : String) return Boolean
      is
      begin
         if not Starts_With (S, I, Last, Open_Mark) then
            return False;
         end if;
         declare
            J : Natural := I + Open_Mark'Length;
         begin
            loop
               if J > Last then
                  return False;
               end if;
               exit when Starts_With (S, J, Last, Closed_Mark);
               if Element (S, J) = '\' then
                  J := J + 1;
               end if;
               J := J + 1;
            end loop;
            Append
              (Buffer,
               HTML_Content_Escape
                 (Unbounded_Slice (S, I, J - 1 + Closed_Mark'Length)));
            I := J - 1 + Closed_Mark'Length;
            return True;
         end;
      end Apply_Quote_MathJax;

      -------------------
      -- Apply_MathJax --
      -------------------

      function Apply_MathJax return Boolean is
      begin
         if Apply_Quote_MathJax ("\[", "\]") then
            return True;
         elsif Apply_Quote_MathJax ("\(", "\)") then
            return True;
         elsif Apply_Quote_MathJax ("$$", "$$") then
            return True;
         else
            return False;
         end if;
      end Apply_MathJax;

   begin
      Bar_Index         := 0;
      Closed_Mark_Index := 0;
      loop
         exit when I > Last;
         if Escape_Flag and then Is_Tilde_Escaping (S, I, Last) then
            I := I + 1;
            if Apply_URL (True) then
               null;
            elsif Context.Format.MathJax_Enabled and then Apply_Escape_MathJax
            then
               null;
            else
               Append (Buffer, HTML_Content_Escape (Element (S, I)));
            end if;
         elsif Element (S, I) = Bar_Character then
            Bar_Index := I;
            exit;
         elsif Closed_Mark_String'Length > 0
           and then Starts_With (S, I, Last, Closed_Mark_String) then
            Closed_Mark_Index := I;
            exit;
         elsif Apply_Emphasis ("**", "**", "<strong>", "</strong>", Open_End)
         then
            null;
         elsif Apply_Emphasis ("//", "//", "<em>", "</em>", Open_End) then
            null;
         elsif Apply_Bracket then
            null;
         elsif Apply_URL then
            null;
         elsif Apply_Line_Break then
            null;
         elsif Apply_Nowiki then
            -- Nowiki must precede image.
            null;
         elsif Apply_Image then
            null;
         elsif Apply_Placeholder then
            -- Placeholder must precede extension in additions.
            null;
         elsif Context.Format.Additions_Enabled and then Apply_Additions then
            null;
         elsif Context.Format.MathJax_Enabled and then Apply_MathJax then
            null;
         else
            Append (Buffer, HTML_Content_Escape (Element (S, I)));
         end if;
         exit when Bar_Index > 0;
         I := I + 1;
      end loop;
      return Buffer;
   end Find_And_Format;

   ------------
   -- Format --
   ------------

   function Format (Context : in out Contexts.Context;
      To_Path               :        access function
        (Name : Unbounded_String) return Unbounded_String;
      Inter_Map : Inter_Maps.Inter_Map; S : Unbounded_String;
      Open_End  : Boolean := True) return Unbounded_String
   is
      Bar_Index         : Natural;
      Closed_Mark_Index : Natural;
   begin
      return Find_And_Format
          (Context, To_Path, Inter_Map, S, 1, Length (S), Open_End, NUL,
           Bar_Index, "", Closed_Mark_Index);
   end Format;

end Licana.Formatters;
