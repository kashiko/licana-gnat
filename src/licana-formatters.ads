with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with Licana.Contexts;       use Licana.Contexts;
with Licana.Inter_Maps;     use Licana.Inter_Maps;

package Licana.Formatters is

   function Is_Tilde_Escaping (S : Unbounded_String; Index : Positive;
      Last                       : Natural) return Boolean;

   function Tilde_Unescape (S : Unbounded_String; First : Positive;
      Last                    : Natural) return Unbounded_String;

   function Find_And_Format (Context : in out Contexts.Context;
      To_Path                        :        access function
        (Name : Unbounded_String) return Unbounded_String;
      Inter_Map : Inter_Maps.Inter_Map; S : Unbounded_String; First : Positive;
      Last :     Natural; Open_End : Boolean; Bar_Character : Character;
      Bar_Index         : out Natural; Closed_Mark_String : String;
      Closed_Mark_Index : out Natural; Escape_Flag : Boolean := True;
      Bracket_Mode      :     Boolean := False) return Unbounded_String;

   function Format (Context : in out Contexts.Context;
      To_Path               :        access function
        (Name : Unbounded_String) return Unbounded_String;
      Inter_Map : Inter_Maps.Inter_Map; S : Unbounded_String;
      Open_End  : Boolean := True) return Unbounded_String;

end Licana.Formatters;
