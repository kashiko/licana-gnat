with Ada.Characters.Latin_1; use Ada.Characters.Latin_1;

package body Licana.HTML_Utils is

   -------------------------
   -- HTML_Content_Escape --
   -------------------------

   function HTML_Content_Escape (C : Character) return String is
   begin
      case C is
         when '&' =>
            return "&amp;";
         when '<' =>
            return "&lt;";
         when '>' =>
            return "&gt;";
         when others =>
            return (1 => C);
      end case;
   end HTML_Content_Escape;

   -------------------------
   -- HTML_Content_Escape --
   -------------------------

   function HTML_Content_Escape (S : String) return String is
      Buffer : Unbounded_String;
   begin
      for I in S'Range loop
         Append (Buffer, HTML_Content_Escape (S (I)));
      end loop;
      return To_String (Buffer);
   end HTML_Content_Escape;

   -------------------------
   -- HTML_Content_Escape --
   -------------------------

   function HTML_Content_Escape (S : Unbounded_String) return Unbounded_String
   is
   begin
      return R : Unbounded_String do
         for I in 1 .. Length (S) loop
            Append (R, HTML_Content_Escape (Element (S, I)));
         end loop;
      end return;
   end HTML_Content_Escape;

   ---------------------------
   -- HTML_Attribute_Escape --
   ---------------------------

   function HTML_Attribute_Escape (C : Character) return String is
   begin
      case C is
         when '&' =>
            return "&amp;";
         when '<' =>
            return "&lt;";
         when '>' =>
            return "&gt;";
         when '"' =>
            return "&quote;";
         when others =>
            return (1 => C);
      end case;
   end HTML_Attribute_Escape;

   ---------------------------
   -- HTML_Attribute_Escape --
   ---------------------------

   function HTML_Attribute_Escape
     (S : Unbounded_String) return Unbounded_String
   is
   begin
      return R : Unbounded_String do
         for I in 1 .. Length (S) loop
            Append (R, HTML_Attribute_Escape (Element (S, I)));
         end loop;
      end return;
   end HTML_Attribute_Escape;

   ------------------------
   -- Create_HTML_Header --
   ------------------------

   function Create_HTML_Header (Style_Sheet_URL : Unbounded_String;
      Title                                     : Unbounded_String;
      MathJax_Enabled : Boolean) return Unbounded_String
   is
   begin
      return R : Unbounded_String do
         Append (R, "<!DOCTYPE html>" & LF);
         Append (R, "<html>" & LF);
         Append (R, "<head>" & LF);
         Append (R, "<meta charset=""UTF-8"" />" & LF);
         if Length (Style_Sheet_URL) > 0 then
            Append (R, "<link rel=""stylesheet"" href=""");
            Append (R, Style_Sheet_URL);
            Append (R, """ />" & LF);
         end if;
         Append (R, "<title>");
         Append (R, HTML_Content_Escape (Title));
         Append (R, "</title>" & LF);
         if MathJax_Enabled then
--              Append (R, "<script type=""text/x-mathjax-config"">" & LF);
--              Append (R, "  MathJax.Hub.Config({" & LF);
--              Append (R, "    tex2jax: {" & LF);
--              Append (R, "      processEscapes: true" & LF);
--              Append (R, "    }" & LF);
--              Append (R, "  });" & LF);
--              Append (R, "</script>" & LF);
            Append
              (R,
               "<script src=""" &
               "https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.5/" &
               "latest.js?config=TeX-MML-AM_CHTML" & """ async></script>" &
               LF);
         end if;
         Append (R, "</head>" & LF);
         Append (R, "<body>" & LF);
      end return;
   end Create_HTML_Header;

   ------------------------
   -- Create_HTML_Footer --
   ------------------------

   function Create_HTML_Footer return Unbounded_String is
   begin
      return R : Unbounded_String do
         Append (R, "</body>" & LF);
         Append (R, "</html>" & LF);
      end return;
   end Create_HTML_Footer;

end Licana.HTML_Utils;
