with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;

package Licana.HTML_Utils is

   function HTML_Content_Escape (C : Character) return String with
      Inline;
   function HTML_Content_Escape (S : String) return String;
   function HTML_Content_Escape (S : Unbounded_String) return Unbounded_String;

   function HTML_Attribute_Escape (C : Character) return String with
      Inline;
   function HTML_Attribute_Escape
     (S : Unbounded_String) return Unbounded_String;

   function Create_HTML_Header (Style_Sheet_URL : Unbounded_String;
      Title                                     : Unbounded_String;
      MathJax_Enabled : Boolean) return Unbounded_String;
   function Create_HTML_Footer return Unbounded_String;

end Licana.HTML_Utils;
