with Ada.Characters.Latin_1; use Ada.Characters.Latin_1;
with Ada.Containers.Vectors;
with Ada.IO_Exceptions;
with Ada.Strings.Hash;
with Ada.Text_IO;            use Ada.Text_IO;
with Licana.Formatters;      use Licana.Formatters;
with Licana.Streams;         use Licana.Streams;
with Licana.Strings;         use Licana.Strings;
with Licana.Text_IO;         use Licana.Text_IO;
with Licana.Utils;           use Licana.Utils;

package body Licana.Inter_Maps is

   ----------
   -- Hash --
   ----------

   function Hash (Item : String_Holders.Holder) return Hash_Type is
   begin
      return Ada.Strings.Hash (Element (Item));
   end Hash;

   ------------------
   -- Set_Defaults --
   ------------------

   procedure Set_Defaults (Self : in out Inter_Map) is
   begin
      Self.Map.Insert
        (To_Holder ("Wiki"), To_Unbounded_String ("http://wiki.c2.com/?"));
      Self.Map.Insert
        (To_Holder ("WikiCreole"),
         To_Unbounded_String ("http://wikicreole.org/wiki/"));
      Self.Map.Insert
        (To_Holder ("DuckDuckGo"),
         To_Unbounded_String ("https://duckduckgo.com/?q="));
      Self.Map.Insert
        (To_Holder ("Wikipedia"),
         To_Unbounded_String ("https://en.wikipedia.org/wiki/"));
   end Set_Defaults;

   ----------
   -- Read --
   ----------

   procedure Read (Self : in out Inter_Map; Path : String) is
      Inter_File   : Ada.Streams.Stream_IO.File_Type;
      Inter_Stream : Stream_Access;
   begin
      Self.Map.Clear;

      Open (Inter_File, In_File, Path);
      Inter_Stream := Stream (Inter_File);
      declare
         Buffer_Size  : constant := 512;
         Input_Stream : Buffered_Stream_Type (Inter_Stream, Buffer_Size);
      begin
         loop
            declare
               Line : Unbounded_String;
               I    : Positive := 1;
            begin
               begin
                  Line := Get_Line (Input_Stream);
               exception
                  when Ada.IO_Exceptions.End_Error =>
                     exit;
               end;
               loop
                  if I > Length (Line) then
                     goto Continue;
                  end if;
                  exit when Element (Line, I) = '|';
                  if Is_Tilde_Escaping (Line, I, Length (Line)) then
                     I := I + 1;
                  end if;
                  I := I + 1;
               end loop;
               if I < 2 then
                  goto Continue;
               end if;
               if I >= Length (Line) then
                  goto Continue;
               end if;
               declare
                  Name : Holder :=
                    To_Holder (To_String (Unbounded_Slice (Line, 1, I - 1)));
                  Path : Unbounded_String :=
                    Unbounded_Slice (Line, I + 1, Length (Line));
               begin
                  if Contains (Self.Map, Name) then
                     Put_Line
                       (Standard_Error,
                        "Warning: Inter Map collision: name = " &
                        Element (Name) & ", path = " & To_String (Path));
                  else
                     Self.Map.Insert (Name, Path);
                  end if;
               end;
            end;
            <<Continue>>
         end loop;
      end;
      Close (Inter_File);
   end Read;

   -----------
   -- Write --
   -----------

   procedure Write (Stream : Stream_Access; Inter_M : Inter_Map) is
      package Unbounded_String_Vectors is new Ada.Containers.Vectors (Positive,
         Unbounded_String);
      use Unbounded_String_Vectors;
      List : Vector;
   begin
      for C in Inter_M.Map.Iterate loop
         declare
            Line : Unbounded_String;
         begin
            Append
              (Line,
               Escape_Tilde_And_Bar (To_Unbounded_String (Element (Key (C)))));
            Append (Line, '|');
            Append (Line, Element (C));
            Append (Line, LF);
            List.Append (Line);
         end;
      end loop;
      declare
         package Sorting is new Generic_Sorting;
         use Sorting;
      begin
         Sort (List);
      end;
      declare
         Buffer_Size   : constant := 512;
         Output_Stream : Buffered_Stream_Type (Stream, Buffer_Size);
      begin
         for Line of List loop
            Put (Output_Stream, Line);
         end loop;
         Flush (Output_Stream);
      end;
   end Write;

   --------------
   -- Get_Path --
   --------------
   function Get_Path (Self : Inter_Map;
      S                    : Unbounded_String) return Unbounded_String
   is
      function Colon_Index (Name : String) return Natural is
      begin
         if Starts_With (S, 1, Length (S), Name) then
            declare
               Index : constant Natural := 1 + Name'Length;
            begin
               if Index <= Length (S) and then Element (S, Index) = ':' then
                  return Index;
               end if;
            end;
         end if;
         return 0;
      end Colon_Index;
   begin
      for C in Self.Map.Iterate loop
         declare
            Name  : constant String := Element (Key (C));
            Colon : Natural         := Colon_Index (Name);
         begin
            if Colon < 1 then
               goto Continue;
            end if;
            if Colon >= Length (S) then
               return Element (C);
            end if;
            return R : Unbounded_String do
               Append (R, Element (C));
               Append (R, Unbounded_Slice (S, Colon + 1, Length (S)));
            end return;
         end;
         <<Continue>>
      end loop;
      return Null_Unbounded_String;
   end Get_Path;

end Licana.Inter_Maps;
