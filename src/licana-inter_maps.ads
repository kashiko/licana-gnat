with Ada.Streams.Stream_IO; use Ada.Streams.Stream_IO;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;

private with Ada.Containers;
private with Ada.Containers.Hashed_Maps;
private with Ada.Containers.Indefinite_Holders;

package Licana.Inter_Maps is

   type Inter_Map is private;

   procedure Set_Defaults (Self : in out Inter_Map);

   procedure Read (Self : in out Inter_Map; Path : String);

   procedure Write (Stream : Stream_Access; Inter_M : Inter_Map);

   function Get_Path (Self : Inter_Map;
      S                    : Unbounded_String) return Unbounded_String;

private

   use Ada.Containers;

   package String_Holders is new Indefinite_Holders (String);
   use String_Holders;

   function Hash (Item : String_Holders.Holder) return Hash_Type;

   package Inter_Hash_Maps is new Ada.Containers.Hashed_Maps
     (String_Holders.Holder, Unbounded_String, Hash, "=");
   use Inter_Hash_Maps;

   type Inter_Map is record
      Map : Inter_Hash_Maps.Map;
   end record;

end Licana.Inter_Maps;
