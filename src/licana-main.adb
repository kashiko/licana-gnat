with Ada.Calendar;             use Ada.Calendar;
with Ada.Calendar.Formatting;
with Ada.Characters.Latin_1;   use Ada.Characters.Latin_1;
with Ada.Containers.Vectors;
with Ada.Directories;          use Ada.Directories;
with Ada.IO_Exceptions;        use Ada.IO_Exceptions;
with Ada.Streams.Stream_IO;    use Ada.Streams.Stream_IO;
with Ada.Strings.Unbounded;    use Ada.Strings.Unbounded;
with Ada.Text_IO;              use Ada.Text_IO;
with GNAT.Command_Line;        use GNAT.Command_Line;
with Licana.Calendar;          use Licana.Calendar;
with Licana.Configs;           use Licana.Configs;
with Licana.Contexts;          use Licana.Contexts;
with Licana.Converters;        use Licana.Converters;
with Licana.Directories;       use Licana.Directories;
with Licana.Inter_Maps;        use Licana.Inter_Maps;
with Licana.Page_Indices;      use Licana.Page_Indices;
with Licana.Streams;           use Licana.Streams;
with Licana.Style_Sheet_Utils; use Licana.Style_Sheet_Utils;
with Licana.Text_IO;           use Licana.Text_IO;
with Licana.Utils;             use Licana.Utils;

procedure Licana.Main is

   ------------------
   -- Main_Command --
   ------------------

   type Main_Command is
     (Initialize_Command, Build_Command, Upgrade_Config_Command,
      Upgrade_Style_Sheet_Command, Undefined_Command);

   function To_Main_Command (S : String) return Main_Command is
   begin
      if S = "init" then
         return Initialize_Command;
      elsif S = "build" then
         return Build_Command;
      elsif S = "upgrade-config" then
         return Upgrade_Config_Command;
      elsif S = "upgrade-style-sheet" then
         return Upgrade_Style_Sheet_Command;
      else
         return Undefined_Command;
      end if;
   end To_Main_Command;

   ---------------
   -- Variables --
   ---------------

   Command : Main_Command := Build_Command;

   Config_File_Name      : constant String := "licana.conf";
   Style_Sheet_File_Name : constant String := "style.css";
   Inter_File_Name       : constant String := "licana.intermap";

   Context : Contexts.Context;
   Force   : Boolean := False;
   Verbose : Boolean := False;

   P_Index : Page_Index;
   Inter_M : Inter_Map;

   ---------------
   -- Show_Help --
   ---------------

   procedure Show_Help is
   begin
      Put_Line ("Usage: licana [OPTION] [COMMAND]");
      New_Line;
      Put_Line ("Options:");
      Put_Line
        ("  -f, --force    ignore file modification time" &
         " and convert all text files");
      Put_Line ("  -h, --help     show this help and exit");
      Put_Line ("  -v, --verbose  be verbose");
      Put_Line ("  -V, --version  show the version of Licana and exit");
      New_Line;
      Put_Line ("Commands:");
      Put_Line
        ("  build                convert new or modified text files to HTML");
      Put_Line
        ("  init                 create licana.conf if it does not exist");
      Put_Line ("  upgrade-config       upgrade licana.conf");
      Put_Line ("  upgrade-style-sheet  upgrade style.css");
      Put_Line
        ("When no command is specified, command will default to build.");
   end Show_Help;

   ------------------
   -- Show_Version --
   ------------------

   procedure Show_Version is
   begin
      Put_Line ("Licana version " & The_Version);
      New_Line;
      Put_Line ("Copyright (C) 2019 Kashiko Koibumi");
      Put_Line ("(BM-NBk7BXQx8AiCpUCLWzgovcpGf93shKN1) <kashiko@tuta.io>.");
      Put_Line ("License GPLv3+: GNU GPL version 3 or later");
      Put_Line ("<https://www.gnu.org/licenses/gpl.html>");
   end Show_Version;

   ----------------------
   -- Find_Config_Path --
   ----------------------

   function Find_Config_Path
     (Current : String := Current_Directory) return String
   is
      C : Unbounded_String := To_Unbounded_String (Current);
   begin
      loop
         declare
            Path : constant String :=
              Compose (To_String (C), Config_File_Name);
         begin
            if Exists (Path) then
               return Path;
            end if;
            C := To_Unbounded_String (Containing_Directory (To_String (C)));
         end;
      end loop;
   end Find_Config_Path;

   -----------------------
   -- Handle_Initialize --
   -----------------------

   procedure Handle_Initialize is
   begin
      begin
         declare
            Config_Path : String := Find_Config_Path;
         begin
            Put_Line ("Error: Already initialized.");
            return;
         end;
      exception
         when Ada.IO_Exceptions.Use_Error =>
            null;
      end;

      if Active (Debug_Logger) then
         Trace (Debug_Logger, "Initialization.");
      end if;

      declare
         Config_Path : String := Compose (Current_Directory, Config_File_Name);
         File        : Ada.Streams.Stream_IO.File_Type;
         S           : Stream_Access;
      begin
         Create (File, Out_File, Config_Path);
         S := Stream (File);
         Write (S, Context);
         Close (File);
      end;
      Put_Line (Config_File_Name & " created.");

      declare
         Style_Sheet_Path : String :=
           Compose (Current_Directory, Style_Sheet_File_Name);
         File : Ada.Streams.Stream_IO.File_Type;
         S    : Stream_Access;
      begin
         Create (File, Out_File, Style_Sheet_Path);
         S := Stream (File);
         declare
            Buffer_Size   : constant := 512;
            Output_Stream : Buffered_Stream_Type (S, Buffer_Size);
         begin
            Put (Output_Stream, Create_Style_Sheet);
            Flush (Output_Stream);
         end;
         Close (File);
      end;
      Put_Line (Style_Sheet_File_Name & " created.");

      declare
         Inter_Path : String := Compose (Current_Directory, Inter_File_Name);
         File       : Ada.Streams.Stream_IO.File_Type;
         S          : Stream_Access;
      begin
         Create (File, Out_File, Inter_Path);
         S := Stream (File);
         Write (S, Inter_M);
         Close (File);
      end;
      Put_Line (Inter_File_Name & " created.");
   end Handle_Initialize;

   -------------------------
   -- Handle_Update_Index --
   -------------------------

   procedure Handle_Update_Index is
   begin
      declare
         Config_Path : String := Find_Config_Path;
         Config_Time : Time   := Modification_Time (Config_Path);
         Index_Time  : Time;

         -----------------------
         -- To_Path_Identical --
         -----------------------

         function To_Path_Identical
           (Name : Unbounded_String) return Unbounded_String
         is
         begin
            return Name;
         end To_Path_Identical;

         -------------------------------
         -- Update_Index_In_Directory --
         -------------------------------

         procedure Update_Index_In_Directory (Directory : String) is
            package Unbounded_String_Vectors is new Ada.Containers.Vectors
              (Positive, Unbounded_String);
            use Unbounded_String_Vectors;
            List   : Vector;
            Search : Search_Type;

            Index_Path : String := Compose (Directory, Page_Index_File_Name);

            Max_Time : Time := Unix_Epoch;

            ------------------------------
            -- Proc_Directory_Find_Time --
            ------------------------------

            procedure Proc_Directory_Find_Time (Directory : String) is
               Text_Path : String := Compose (Directory, "index.txt");
            begin
               if not Exists (Text_Path) then
                  return;
               end if;
               declare
                  T : Time := Modification_Time (Text_Path);
               begin
                  if T > Max_Time then
                     declare
                        Text_File    : Ada.Streams.Stream_IO.File_Type;
                        Text_Stream  : Stream_Access;
                        Page_Context : Contexts.Context := Copy (Context);
                     begin
                        Open (Text_File, In_File, Text_Path);
                        Text_Stream := Stream (Text_File);
                        Convert
                          (Page_Context, To_Path_Identical'Access, Inter_M,
                           Text_Stream, null);
                        Close (Text_File);
                        if Length (Page_Context.Title) > 0 then
                           if Element
                               (P_Index, Containing_Directory (Directory),
                                Page_Context.Title) =
                             Simple_Name (Directory) & '/' then
                              return;
                           end if;
                        end if;
                     end;
                     Max_Time := T;
                  end if;
               end;
            end Proc_Directory_Find_Time;

            --------------------
            -- Proc_Directory --
            --------------------

            procedure Proc_Directory (Directory : String) is
               Text_Path : String := Compose (Directory, "index.txt");
            begin
               if not Exists (Text_Path) then
                  return;
               end if;
               declare
                  Text_File    : Ada.Streams.Stream_IO.File_Type;
                  Text_Stream  : Stream_Access;
                  Page_Context : Contexts.Context := Copy (Context);
               begin
                  if Active (Debug_Logger) then
                     Trace (Debug_Logger, "Index " & Directory);
                  end if;
                  if Verbose then
                     Put_Line ("Index " & Directory);
                  end if;
                  Open (Text_File, In_File, Text_Path);
                  Text_Stream := Stream (Text_File);
                  Convert
                    (Page_Context, To_Path_Identical'Access, Inter_M,
                     Text_Stream, null);
                  Close (Text_File);
                  if Length (Page_Context.Title) > 0 then
                     declare
                        Buffer : Unbounded_String;
                     begin
                        Append
                          (Buffer, Escape_Tilde_And_Bar (Page_Context.Title));
                        Append (Buffer, '|');
                        Append (Buffer, Simple_Name (Directory));
                        Append (Buffer, '/');
                        Append (Buffer, LF);
                        List.Append (Buffer);
                     end;
                  end if;
               end;
            end Proc_Directory;

         begin
            Apply_To_Directories (Directory, Proc_Directory_Find_Time'Access);

            Start_Search
              (Search, Directory, "",
               (Ordinary_File => True, others => False));
            while More_Entries (Search) loop
               declare
                  Directory_Entry : Directory_Entry_Type;
               begin
                  Get_Next_Entry (Search, Directory_Entry);
                  declare
                     Name : String := Simple_Name (Directory_Entry);
                  begin
                     if Name'Length <= 4
                       or else Name (Name'Last - 3 .. Name'Last) /= ".txt" then
                        goto Find_Time_Continue;
                     end if;
                     declare
                        T : Time :=
                          Modification_Time (Full_Name (Directory_Entry));
                     begin
                        if T > Max_Time then
                           declare
                              Text_File    : Ada.Streams.Stream_IO.File_Type;
                              Text_Stream  : Stream_Access;
                              Page_Context : Contexts.Context :=
                                Copy (Context);
                              HTML_Filename : String :=
                                Base_Name (Name) & ".html";
                           begin
                              Open
                                (Text_File, In_File,
                                 Full_Name (Directory_Entry));
                              Text_Stream := Stream (Text_File);
                              Convert
                                (Page_Context, To_Path_Identical'Access,
                                 Inter_M, Text_Stream, null);
                              Close (Text_File);
                              if Length (Page_Context.Title) > 0 then
                                 if Element
                                     (P_Index, Directory, Page_Context.Title) =
                                   HTML_Filename then
                                    goto Find_Time_Continue;
                                 end if;
                              end if;
                           end;
                           Max_Time := T;
                        end if;
                     end;
                  end;
               end;
               <<Find_Time_Continue>>
            end loop;
            End_Search (Search);

            if not Force and then Exists (Index_Path) then
               declare
                  T : Time := Modification_Time (Index_Path);
               begin
                  if T >= Config_Time and then T >= Max_Time then
                     return;
                  end if;
               end;
            end if;

            Apply_To_Directories (Directory, Proc_Directory'Access);

            Start_Search
              (Search, Directory, "",
               (Ordinary_File => True, others => False));
            while More_Entries (Search) loop
               declare
                  Directory_Entry : Directory_Entry_Type;
               begin
                  Get_Next_Entry (Search, Directory_Entry);
                  declare
                     Name : String := Simple_Name (Directory_Entry);
                  begin
                     if Name'Length <= 4
                       or else Name (Name'Last - 3 .. Name'Last) /= ".txt" then
                        goto Continue;
                     end if;
                     declare
                        Text_File    : Ada.Streams.Stream_IO.File_Type;
                        Text_Stream  : Stream_Access;
                        Page_Context : Contexts.Context := Copy (Context);
                     begin
                        if Active (Debug_Logger) then
                           Trace
                             (Debug_Logger,
                              "Index " & Full_Name (Directory_Entry));
                        end if;
                        if Verbose then
                           Put_Line ("Index " & Full_Name (Directory_Entry));
                        end if;
                        Open (Text_File, In_File, Full_Name (Directory_Entry));
                        Text_Stream := Stream (Text_File);
                        Convert
                          (Page_Context, To_Path_Identical'Access, Inter_M,
                           Text_Stream, null);
                        Close (Text_File);
                        if Length (Page_Context.Title) > 0 then
                           declare
                              Buffer        : Unbounded_String;
                              HTML_Filename : String :=
                                Base_Name (Name) & ".html";
                           begin
                              Append
                                (Buffer,
                                 Escape_Tilde_And_Bar (Page_Context.Title));
                              Append (Buffer, '|');
                              Append (Buffer, Escape_Percent (HTML_Filename));
                              Append (Buffer, LF);
                              List.Append (Buffer);
                           end;
                        end if;
                     end;
                  end;
               end;
               <<Continue>>
            end loop;
            End_Search (Search);

            declare
               package Sorting is new Generic_Sorting;
               use Sorting;
            begin
               Sort (List);
            end;

            declare
               Index_File   : Ada.Streams.Stream_IO.File_Type;
               Index_Stream : Stream_Access;
            begin
               Create (Index_File, Out_File, Index_Path);
               Index_Stream := Stream (Index_File);
               declare
                  Buffer_Size   : constant := 512;
                  Output_Stream : Buffered_Stream_Type (Index_Stream,
                     Buffer_Size);
               begin
                  for Line of List loop
                     Put (Output_Stream, Line);
                  end loop;
                  Flush (Output_Stream);
               end;
               Close (Index_File);
            end;
         end Update_Index_In_Directory;

      begin
         if Active (Debug_Logger) then
            Trace (Debug_Logger, "Config path: " & Config_Path);
         end if;
         Read (Config_Path, Context);
         Read (P_Index, Containing_Directory (Config_Path), Index_Time);
         declare
            Inter_Path : String :=
              Compose (Containing_Directory (Config_Path), Inter_File_Name);
         begin
            if Exists (Inter_Path) then
               Read (Inter_M, Inter_Path);
            end if;
         end;

         Apply_To_Directories_Recursively
           (Containing_Directory (Config_Path),
            Update_Index_In_Directory'Access);
      end;
   exception
      when Ada.IO_Exceptions.Use_Error =>
         Put_Line ("Error: Cannot find config file.");
         Put_Line ("Do 'licana init' first.");
         raise;
   end Handle_Update_Index;

   ------------------
   -- Handle_Build --
   ------------------

   procedure Handle_Build (Read_Configs : Boolean := True) is
   begin
      declare

         Config_Path : String := Find_Config_Path;
         Config_Time : Time   := Modification_Time (Config_Path);
         Index_Time  : Time;
         Inter_Time  : Time   := Unix_Epoch;

         --------------------------
         -- Convert_In_Directory --
         --------------------------

         procedure Convert_In_Directory (Directory : String) is
            Search : Search_Type;
         begin
            Start_Search
              (Search, Directory, "",
               (Ordinary_File => True, others => False));
            while More_Entries (Search) loop
               declare
                  Directory_Entry : Directory_Entry_Type;
               begin
                  Get_Next_Entry (Search, Directory_Entry);
                  declare
                     Name : String := Simple_Name (Directory_Entry);
                  begin
                     if Name'Length <= 4
                       or else Name (Name'Last - 3 .. Name'Last) /= ".txt" then
                        goto Continue;
                     end if;
                     declare
                        HTML_Path : String :=
                          Compose
                            (Containing_Directory
                               (Full_Name (Directory_Entry)),
                             Base_Name (Simple_Name (Directory_Entry)) &
                             ".html");
                     begin
                        if not Force and then Exists (HTML_Path) then
                           declare
                              HTML_Time : Time :=
                                Modification_Time (HTML_Path);
                           begin
                              if Modification_Time (Directory_Entry) <=
                                HTML_Time
                                and then HTML_Time >= Config_Time
                                and then HTML_Time >= Index_Time
                                and then HTML_Time >= Inter_Time then
                                 goto Continue;
                              end if;
                           end;
                        end if;
                        declare
                           Text_File    : Ada.Streams.Stream_IO.File_Type;
                           Text_Stream  : Stream_Access;
                           HTML_File    : Ada.Streams.Stream_IO.File_Type;
                           HTML_Stream  : Stream_Access;
                           Page_Context : Contexts.Context := Copy (Context);
                        begin
                           if Active (Debug_Logger) then
                              Trace
                                (Debug_Logger,
                                 "Convert " & Full_Name (Directory_Entry) &
                                 " to " & HTML_Path);
                           end if;
                           if Verbose then
                              Put_Line
                                ("Convert " & Full_Name (Directory_Entry) &
                                 " to " & HTML_Path);
                           end if;
                           Open
                             (Text_File, In_File, Full_Name (Directory_Entry));
                           Text_Stream := Stream (Text_File);
                           Create (HTML_File, Out_File, HTML_Path);
                           HTML_Stream := Stream (HTML_File);
                           declare
                              function To_Path
                                (Name : Unbounded_String)
                                 return Unbounded_String
                              is
                              begin
                                 return Get_Path
                                     (P_Index, Directory, Name, Context);
                              end To_Path;
                           begin
                              Convert
                                (Page_Context, To_Path'Access, Inter_M,
                                 Text_Stream, HTML_Stream);
                           end;
                           Close (HTML_File);
                           Close (Text_File);
                        end;
                     end;
                  end;
               end;
               <<Continue>>
            end loop;
            End_Search (Search);
         end Convert_In_Directory;

      begin
         if Active (Debug_Logger) then
            Trace (Debug_Logger, "Config path: " & Config_Path);
         end if;
         if Read_Configs then
            Read (Config_Path, Context);
         end if;
         Read (P_Index, Containing_Directory (Config_Path), Index_Time);
         if Read_Configs then
            declare
               Inter_Path : String :=
                 Compose (Containing_Directory (Config_Path), Inter_File_Name);
            begin
               if Exists (Inter_Path) then
                  Inter_Time := Modification_Time (Inter_Path);
                  Read (Inter_M, Inter_Path);
               end if;
            end;
         end if;

         Apply_To_Directories_Recursively
           (Containing_Directory (Config_Path), Convert_In_Directory'Access);
      end;
   exception
      when Ada.IO_Exceptions.Use_Error =>
         Put_Line ("Error: Cannot find config file.");
         Put_Line ("Do 'licana init' first.");
         raise;
   end Handle_Build;

   ---------------------------
   -- Handle_Upgrade_Config --
   ---------------------------

   procedure Handle_Upgrade_Config is
   begin
      declare
         Config_Path : String := Find_Config_Path;
         File        : Ada.Streams.Stream_IO.File_Type;
         S           : Stream_Access;
      begin
         if Active (Debug_Logger) then
            Trace (Debug_Logger, "Config path: " & Config_Path);
         end if;
         Read (Config_Path, Context);
         Create (File, Out_File, Config_Path);
         S := Stream (File);
         Write (S, Context);
         Close (File);
      end;
      Put_Line ("Config file upgraded.");
   exception
      when Ada.IO_Exceptions.Use_Error =>
         Put_Line ("Error: Cannot find config file.");
         Put_Line ("Do 'licana init' first.");
   end Handle_Upgrade_Config;

   --------------------------------
   -- Handle_Upgrade_Style_Sheet --
   --------------------------------

   procedure Handle_Upgrade_Style_Sheet is
   begin
      declare
         Config_Path : String := Find_Config_Path;
      begin
         if Active (Debug_Logger) then
            Trace (Debug_Logger, "Config path: " & Config_Path);
         end if;
         declare
            Style_Sheet_Path : String :=
              Compose
                (Containing_Directory (Config_Path), Style_Sheet_File_Name);
            File : Ada.Streams.Stream_IO.File_Type;
            S    : Stream_Access;
         begin
            Create (File, Out_File, Style_Sheet_Path);
            S := Stream (File);
            declare
               Buffer_Size   : constant := 512;
               Output_Stream : Buffered_Stream_Type (S, Buffer_Size);
            begin
               Put (Output_Stream, Create_Style_Sheet);
               Flush (Output_Stream);
            end;
            Close (File);
         end;
      end;
      Put_Line ("Style sheet file upgraded.");
   exception
      when Ada.IO_Exceptions.Use_Error =>
         Put_Line ("Error: Cannot find config file.");
         Put_Line ("Do 'licana init' first.");
   end Handle_Upgrade_Style_Sheet;

   ----------------------
   -- Handle_Undefined --
   ----------------------

   procedure Handle_Undefined is
   begin
      Put_Line ("Error: Undefined command.");
   end Handle_Undefined;

begin

   ------------------------
   -- Begin main program --
   ------------------------

   Parse_Config_File;

   if Active (Debug_Logger) then
      Trace (Debug_Logger, "Begin Licana.");
   end if;

   ------------------
   -- Set defaults --
   ------------------

   Set_Defaults (Context);
   Set_Defaults (Inter_M);

   ------------------------
   -- Parse command line --
   ------------------------

   loop
      case Getopt ("f -force h -help v -verbose V -version") is
         when 'f' =>
            Force := True;
         when 'h' =>
            Show_Help;
            return;
         when 'v' =>
            Verbose := True;
         when 'V' =>
            Show_Version;
            return;
         when '-' =>
            if Full_Switch = "-force" then
               Force := True;
            elsif Full_Switch = "-help" then
               Show_Help;
               return;
            elsif Full_Switch = "-verbose" then
               Verbose := True;
            elsif Full_Switch = "-version" then
               Show_Version;
               return;
            end if;
         when NUL =>
            exit;
         when others =>
            null;
      end case;
   end loop;

   declare
      S : constant String := Get_Argument;
   begin
      if S'Length > 0 then
         Command := To_Main_Command (S);
      end if;
   end;
   if Active (Debug_Logger) then
      Trace (Debug_Logger, "Command: " & Main_Command'Image (Command));
   end if;

   ----------------------
   -- Dispatch command --
   ----------------------

   case Command is
      when Initialize_Command =>
         Handle_Initialize;
      when Build_Command =>
         begin
            Handle_Update_Index;
            Handle_Build (False);
         exception
            when Ada.IO_Exceptions.Use_Error =>
               null;
         end;
      when Upgrade_Config_Command =>
         Handle_Upgrade_Config;
      when Upgrade_Style_Sheet_Command =>
         Handle_Upgrade_Style_Sheet;
      when Undefined_Command =>
         Handle_Undefined;
   end case;

   ----------------------
   -- End main program --
   ----------------------

   if Active (Debug_Logger) then
      Trace (Debug_Logger, "End Licana.");
   end if;

end Licana.Main;
