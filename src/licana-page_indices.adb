with Ada.Directories;       use Ada.Directories;
with Ada.IO_Exceptions;
with Ada.Streams.Stream_IO; use Ada.Streams.Stream_IO;
with Ada.Text_IO;           use Ada.Text_IO;
with Licana.Calendar;       use Licana.Calendar;
with Licana.Directories;    use Licana.Directories;
with Licana.Formatters;     use Licana.Formatters;
with Licana.Streams;        use Licana.Streams;
with Licana.Text_IO;        use Licana.Text_IO;

package body Licana.Page_Indices is

   ---------------
   -- Constants --
   ---------------

   -- TODO Remove this restriction.
   -- Use Unbounded_String to process directories to protect stacks.
   Max_String_Length : constant := 1024;

   ----------
   -- Read --
   ----------

   procedure Read (Self : in out Page_Index; Directory : String;
      M_Time            :    out Time)
   is

      -----------------------
      -- Read_In_Directory --
      -----------------------

      procedure Read_In_Directory (Directory : String) is
         Index_Path : String := Compose (Directory, Page_Index_File_Name);
      begin
         if not Exists (Index_Path) then
            return;
         end if;
         declare
            T : Time := Modification_Time (Index_Path);
         begin
            if T > M_Time then
               M_Time := T;
            end if;
         end;
         declare
            Map          : Index_Hash_Maps.Map;
            Index_File   : Ada.Streams.Stream_IO.File_Type;
            Index_Stream : Stream_Access;
         begin
            Open (Index_File, In_File, Index_Path);
            Index_Stream := Stream (Index_File);
            declare
               Buffer_Size  : constant := 512;
               Input_Stream : Buffered_Stream_Type (Index_Stream, Buffer_Size);
            begin
               loop
                  declare
                     Line : Unbounded_String;
                     I    : Positive := 1;
                  begin
                     begin
                        Line := Get_Line (Input_Stream);
                     exception
                        when Ada.IO_Exceptions.End_Error =>
                           exit;
                     end;
                     loop
                        if I > Length (Line) then
                           goto Continue;
                        end if;
                        exit when Element (Line, I) = '|';
                        if Is_Tilde_Escaping (Line, I, Length (Line)) then
                           I := I + 1;
                        end if;
                        I := I + 1;
                     end loop;
                     if I < 2 then
                        goto Continue;
                     end if;
                     if I >= Length (Line) then
                        goto Continue;
                     end if;
                     declare
                        Name : Unbounded_String :=
                          Unbounded_Slice (Line, 1, I - 1);
                        Path : Unbounded_String :=
                          Unbounded_Slice (Line, I + 1, Length (Line));
                     begin
                        if Contains (Map, Name) then
                           Put_Line
                             (Standard_Error,
                              "Warning: Page name index collision: name = " &
                              To_String (Name) & ", path = " &
                              To_String (Path));
                        else
                           Map.Insert (Name, Path);
                        end if;
                     end;
                  end;
                  <<Continue>>
               end loop;
            end;
            Close (Index_File);
            Self.Map.Insert (To_Unbounded_String (Directory), Map);
         end;
      end Read_In_Directory;

   begin
      M_Time := Unix_Epoch;
      Self.Map.Clear;
      Apply_To_Directories_Recursively (Directory, Read_In_Directory'Access);
   end Read;

   ----------------------
   -- Get_Next_Segment --
   ----------------------

   procedure Get_Next_Segment (S : Unbounded_String; Index : in out Positive;
      Segment : out Unbounded_String; Fragment : out Unbounded_String)
   is
   begin
      if Element (S, Index) = '/' then
         Index   := Index + 1;
         Segment := To_Unbounded_String ("/");
         return;
      end if;
      declare
         I : Natural := Index;
      begin
         loop
            if I > Length (S) then
               Segment := Unbounded_Slice (S, Index, I - 1);
               Index   := I;
               return;
            end if;
            declare
               C : constant Character := Element (S, I);
            begin
               if C = '#' then
                  Segment  := Unbounded_Slice (S, Index, I - 1);
                  Fragment := Unbounded_Slice (S, I, Length (S));
                  Index    := Length (S) + 1;
                  return;
               end if;
               if C = '/' then
                  Segment := Unbounded_Slice (S, Index, I - 1);
                  Index   := I + 1;
                  return;
               end if;
            end;
            I := I + 1;
         end loop;
      end;
   end Get_Next_Segment;

   --------------------------
   -- Containing_Directory --
   --------------------------

   function Containing_Directory
     (Name : Unbounded_String) return Unbounded_String
   is
   begin
      if Length (Name) > Max_String_Length then
         raise Constraint_Error with "Name is too long.";
      end if;
      return To_Unbounded_String (Containing_Directory (To_String (Name)));
   end Containing_Directory;

   -------------
   -- Compose --
   -------------

   function Compose
     (Containing_Directory : Unbounded_String := Null_Unbounded_String;
      Name                 : Unbounded_String;
      Extension            : Unbounded_String := Null_Unbounded_String)
      return Unbounded_String
   is
   begin
      if Length (Containing_Directory) + Length (Name) + Length (Extension) >
        Max_String_Length then
         raise Constraint_Error with "Composed string will be too long.";
      end if;
      return To_Unbounded_String
          (Compose
             (To_String (Containing_Directory), To_String (Name),
              To_String (Extension)));
   end Compose;

   --------------
   -- Get_Path --
   --------------

   function Get_Path (Self : Page_Index; Directory : String;
      S : Unbounded_String; Context : Contexts.Context) return Unbounded_String
   is
      Buffer : Unbounded_String;
      Index  : Positive         := 1;
      Dir    : Unbounded_String := To_Unbounded_String (Directory);
   begin
      loop
         if Index > Length (S) then
            return Buffer;
         end if;
         declare
            Segment  : Unbounded_String;
            Fragment : Unbounded_String;
         begin
            Get_Next_Segment (S, Index, Segment, Fragment);
            if Length (Segment) < 1 then
               if Length (Fragment) < 1 then
                  return Null_Unbounded_String;
               else
                  if not Context.Format.Index_Filename_Omission_Enabled then
                     if Length (Buffer) > 0
                       and then Element (Buffer, Length (Buffer)) = '/' then
                        Append (Buffer, "index.html");
                     end if;
                  end if;
                  return Buffer & Fragment;
               end if;
            end if;
            if Segment = "/" and then Length (Fragment) < 1 then
               Append (Buffer, Context.Format.Document_Root);
               goto Continue;
            end if;
            declare
               C : constant Directory_Hash_Maps.Cursor := Self.Map.Find (Dir);
            begin
               if C = Directory_Hash_Maps.No_Element then
                  return Null_Unbounded_String;
               end if;
               declare
                  Map : constant Index_Hash_Maps.Map    := Element (C);
                  IC  : constant Index_Hash_Maps.Cursor := Map.Find (Segment);
               begin
                  if IC /= Index_Hash_Maps.No_Element then
                     declare
                        P : constant Unbounded_String := Element (IC);
                     begin
                        Append (Buffer, P);
                        if Element (P, Length (P)) = '/' then
                           Dir :=
                             Compose
                               (Dir, Unbounded_Slice (P, 1, Length (P) - 1));
                           if not Context.Format
                               .Index_Filename_Omission_Enabled then
                              if Index > Length (S) then
                                 Append (Buffer, "index.html");
                              end if;
                           end if;
                        else
                           Dir := Compose (Dir, P);
                        end if;
                     end;
                     Append (Buffer, Fragment);
                     goto Continue;
                  end if;
               end;
            end;
            if Segment = ".." and then Length (Fragment) < 1 then
               begin
                  declare
                     D : constant Unbounded_String :=
                       Containing_Directory (Dir);
                  begin
                     if Self.Map.Contains (D) then
                        Dir := D;
                     else
                        return Null_Unbounded_String;
                     end if;
                  end;
               exception
                  when Ada.IO_Exceptions.Use_Error =>
                     return Null_Unbounded_String;
               end;
               Append (Buffer, Segment);
               Append (Buffer, '/');
               goto Continue;
            end if;
            declare
               D : constant Unbounded_String := Compose (Dir, Segment);
            begin
               if Self.Map.Contains (D) then
                  Dir := D;
                  Append (Buffer, Segment);
                  Append (Buffer, '/');
                  if not Context.Format.Index_Filename_Omission_Enabled then
                     if Index > Length (S) then
                        Append (Buffer, "index.html");
                     end if;
                  end if;
                  Append (Buffer, Fragment);
                  goto Continue;
               end if;
            end;
         end;
         <<Continue>>
      end loop;
   end Get_Path;

   -------------
   -- Element --
   -------------

   function Element (Self : Page_Index; Directory : String;
      S                   : Unbounded_String) return Unbounded_String
   is
      C : constant Directory_Hash_Maps.Cursor :=
        Self.Map.Find (To_Unbounded_String (Directory));
   begin
      if C = Directory_Hash_Maps.No_Element then
         return Null_Unbounded_String;
      end if;
      declare
         Map : constant Index_Hash_Maps.Map    := Element (C);
         IC  : constant Index_Hash_Maps.Cursor := Map.Find (S);
      begin
         if IC = Index_Hash_Maps.No_Element then
            return Null_Unbounded_String;
         end if;
         return Element (IC);
      end;
   end Element;

end Licana.Page_Indices;
