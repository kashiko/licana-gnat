with Ada.Calendar;          use Ada.Calendar;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with Licana.Contexts;       use Licana.Contexts;

private with Ada.Containers;
private with Ada.Containers.Hashed_Maps;
private with Ada.Strings.Unbounded.Hash;

package Licana.Page_Indices is

   type Page_Index is private;

   Page_Index_File_Name : constant String := "licana.index";

   procedure Read (Self : in out Page_Index; Directory : String;
      M_Time            :    out Time);

   function Get_Path (Self : Page_Index; Directory : String;
      S                    : Unbounded_String;
      Context              : Contexts.Context) return Unbounded_String;

   function Element (Self : Page_Index; Directory : String;
      S                   : Unbounded_String) return Unbounded_String;

private

   use Ada.Containers;

   package Index_Hash_Maps is new Ada.Containers.Hashed_Maps (Unbounded_String,
      Unbounded_String, Ada.Strings.Unbounded.Hash, "=");
   use Index_Hash_Maps;

   package Directory_Hash_Maps is new Ada.Containers.Hashed_Maps
     (Unbounded_String, Index_Hash_Maps.Map, Ada.Strings.Unbounded.Hash, "=");
   use Directory_Hash_Maps;

   type Page_Index is record
      Map : Directory_Hash_Maps.Map;
   end record;

end Licana.Page_Indices;
