with Ada.Streams;           use Ada.Streams;
with Ada.Streams.Stream_IO; use Ada.Streams.Stream_IO;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;

package Licana.Streams is

   type Buffered_Stream_Type (Stream : Stream_Access;
      Buffer_Size                    : Stream_Element_Count) is
     new Root_Stream_Type with private;

   procedure Read (Stream : in out Buffered_Stream_Type;
      Item :    out Stream_Element_Array; Last : out Stream_Element_Offset);

   procedure Write (Stream : in out Buffered_Stream_Type;
      Item                 :        Stream_Element_Array);

   procedure Unget (Stream : in out Buffered_Stream_Type);

   procedure Flush (Stream : in out Buffered_Stream_Type);

private

   type Buffered_Stream_Type (Stream : Stream_Access;
      Buffer_Size : Stream_Element_Count) is new Root_Stream_Type with record
      Buffer : Stream_Element_Array (1 .. Buffer_Size);
      Last   : Stream_Element_Offset := 0;
      Index  : Stream_Element_Offset := 1;
   end record;

end Licana.Streams;
