with Ada.Characters.Latin_1; use Ada.Characters.Latin_1;

package body Licana.Strings is

   -------------------
   -- Is_Whitespace --
   -------------------

   function Is_Whitespace (C : Character) return Boolean is
   begin
      return C = ' ' or else C = HT;
   end Is_Whitespace;

   ------------------------------
   -- Is_Whitespace_Or_Newline --
   ------------------------------

   function Is_Whitespace_Or_Newline (C : Character) return Boolean is
   begin
      return C = ' ' or else C = HT or else C = LF;
   end Is_Whitespace_Or_Newline;

   ----------------------
   -- Trim_Whitespaces --
   ----------------------

   function Trim_Whitespaces (S : Unbounded_String) return Unbounded_String is
      I : Natural := 1;
      J : Natural := Length (S);
   begin
      loop
         if I > J then
            return Null_Unbounded_String;
         end if;
         exit when not Is_Whitespace (Element (S, I));
         I := I + 1;
      end loop;
      loop
         exit when not Is_Whitespace (Element (S, J));
         J := J - 1;
      end loop;
      if I = 1 and then J = Length (S) then
         return S;
      end if;
      return Unbounded_Slice (S, I, J);
   end Trim_Whitespaces;

   -----------------------------------
   -- Trim_Whitespaces_And_Newlines --
   -----------------------------------

   function Trim_Whitespaces_And_Newlines
     (S : Unbounded_String) return Unbounded_String
   is
      I : Natural := 1;
      J : Natural := Length (S);
   begin
      loop
         if I > J then
            return Null_Unbounded_String;
         end if;
         exit when not Is_Whitespace_Or_Newline (Element (S, I));
         I := I + 1;
      end loop;
      loop
         exit when not Is_Whitespace_Or_Newline (Element (S, J));
         J := J - 1;
      end loop;
      if I = 1 and then J = Length (S) then
         return S;
      end if;
      return Unbounded_Slice (S, I, J);
   end Trim_Whitespaces_And_Newlines;

   -----------------
   -- Starts_With --
   -----------------

   function Starts_With (S : Unbounded_String; First : Positive;
      Last                 : Natural; Prefix : String) return Boolean
   is
   begin
      if First + Prefix'Length - 1 > Last then
         return False;
      end if;
      for I in 1 .. Prefix'Length loop
         if Element (S, First + I - 1) /= Prefix (Prefix'First + I - 1) then
            return False;
         end if;
      end loop;
      return True;
   end Starts_With;

   ----------
   -- Find --
   ----------

   function Find (S : Unbounded_String; First : Positive; Last : Natural;
      C             : Character) return Natural
   is
      I : Positive := First;
   begin
      loop
         if I > Last then
            return 0;
         end if;
         if Element (S, I) = C then
            return I;
         end if;
         I := I + 1;
      end loop;
   end Find;

end Licana.Strings;
