with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;

package Licana.Strings is

   function Is_Whitespace (C : Character) return Boolean with
      Inline;

   function Is_Whitespace_Or_Newline (C : Character) return Boolean with
      Inline;

   function Trim_Whitespaces (S : Unbounded_String) return Unbounded_String;

   function Trim_Whitespaces_And_Newlines
     (S : Unbounded_String) return Unbounded_String;

   function Starts_With (S : Unbounded_String; First : Positive;
      Last                 : Natural; Prefix : String) return Boolean;

   function Find (S : Unbounded_String; First : Positive; Last : Natural;
      C             : Character) return Natural;

end Licana.Strings;
