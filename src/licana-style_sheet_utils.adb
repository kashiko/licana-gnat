with Ada.Characters.Latin_1; use Ada.Characters.Latin_1;

package body Licana.Style_Sheet_Utils is

   ----------------
   -- Create_CSS --
   ----------------

   function Create_Style_Sheet return Unbounded_String is
   begin
      return R : Unbounded_String do
         Append (R, "h1, h2, h3 {" & LF);
         Append (R, "  border-bottom: 1px solid DimGray;" & LF);
         Append (R, "}" & LF);
         Append (R, "table {" & LF);
         Append (R, "  border-collapse: collapse;" & LF);
         Append (R, "  margin: 4px 12px;" & LF);
         Append (R, "}" & LF);
         Append (R, "table, th, td {" & LF);
         Append (R, "  border: 1px solid DimGray;" & LF);
         Append (R, "  padding: 4px;" & LF);
         Append (R, "}" & LF);
         Append (R, "th {" & LF);
         Append (R, "  background-color: Gainsboro;" & LF);
         Append (R, "}" & LF);
         Append (R, "pre {" & LF);
         Append (R, "  margin: 4px 12px;" & LF);
         Append (R, "  background-color: Gainsboro;" & LF);
         Append (R, "  border: 1px solid LightGray;" & LF);
         Append (R, "  padding: 4px;" & LF);
         Append (R, "}" & LF);
         Append (R, "code {" & LF);
         Append (R, "  background-color: Gainsboro;" & LF);
         Append (R, "  border: 1px solid LightGray;" & LF);
         Append (R, "  padding: 2px;" & LF);
         Append (R, "  border-radius: 4px;" & LF);
         Append (R, "}" & LF);
         Append (R, "div.indent {" & LF);
         Append (R, "  margin-left: 2em;" & LF);
         Append (R, "}" & LF);
      end return;
   end Create_Style_Sheet;

end Licana.Style_Sheet_Utils;
