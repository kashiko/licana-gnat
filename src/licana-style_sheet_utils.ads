with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;

package Licana.Style_Sheet_Utils is

   function Create_Style_Sheet return Unbounded_String;

end Licana.Style_Sheet_Utils;
