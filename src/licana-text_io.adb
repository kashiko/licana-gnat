with Ada.Characters.Latin_1; use Ada.Characters.Latin_1;
with Ada.IO_Exceptions;      use Ada.IO_Exceptions;

package body Licana.Text_IO is

   --------------
   -- Get_Line --
   --------------

   function Get_Line
     (Stream : in out Buffered_Stream_Type) return Unbounded_String
   is
      Line : Unbounded_String;
   begin
      loop
         declare
            C : Character;
         begin
            Character'Read (Stream'Access, C);
            if C = CR then
               declare
                  Next : Character;
               begin
                  Character'Read (Stream'Access, Next);
                  if Next /= LF then
                     Unget (Stream);
                  end if;
               end;
               return Line;
            elsif C = LF then
               return Line;
            end if;
            Append (Line, C);
         end;
      end loop;
   exception
      when Ada.IO_Exceptions.End_Error =>
         if Length (Line) > 0 then
            return Line;
         end if;
         raise;
   end Get_Line;

   ---------
   -- Put --
   ---------

   procedure Put (Stream : in out Buffered_Stream_Type; C : Character) is
   begin
      Character'Write (Stream'Access, C);
   end Put;

   ---------
   -- Put --
   ---------

   procedure Put (Stream : in out Buffered_Stream_Type; S : String) is
   begin
      String'Write (Stream'Access, S);
   end Put;

   ---------
   -- Put --
   ---------

   procedure Put (Stream : in out Buffered_Stream_Type; S : Unbounded_String;
      Chunk_Size         :        Positive := Default_Chunk_Size)
   is
      I : Natural := 0;
   begin
      while I < Length (S) loop
         declare
            L : constant Natural := Natural'Min (Chunk_Size, Length (S) - I);
         begin
            String'Write (Stream'Access, Slice (S, 1 + I, 1 + I + L - 1));
            I := I + L;
         end;
      end loop;
   end Put;

end Licana.Text_IO;
