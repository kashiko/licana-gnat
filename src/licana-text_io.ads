with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with Licana.Streams;        use Licana.Streams;

package Licana.Text_IO is

   function Get_Line
     (Stream : in out Buffered_Stream_Type) return Unbounded_String;

   procedure Put (Stream : in out Buffered_Stream_Type; C : Character) with
      Inline;
   procedure Put (Stream : in out Buffered_Stream_Type; S : String) with
      Inline;

   Default_Chunk_Size : constant := 512;

   procedure Put (Stream : in out Buffered_Stream_Type; S : Unbounded_String;
      Chunk_Size         :        Positive := Default_Chunk_Size);

end Licana.Text_IO;
