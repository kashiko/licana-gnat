with Interfaces;     use Interfaces;
with Licana.Strings; use Licana.Strings;

package body Licana.URL_Utils is

   -------------------------------
   -- Is_URL_Reserved_Character --
   -------------------------------

   function Is_URL_Reserved_Character (C : Character) return Boolean is
      Reserved_Characters : constant String := "!*'();:@&=+$,/?#[]";
   begin
      for RC of Reserved_Characters loop
         if C = RC then
            return True;
         end if;
      end loop;
      return False;
   end Is_URL_Reserved_Character;

   ---------------------------------
   -- Is_URL_Unreserved_Character --
   ---------------------------------

   function Is_URL_Unreserved_Character (C : Character) return Boolean is
   begin
      if C >= 'A' and then C <= 'Z' then
         return True;
      elsif C >= 'a' and then C <= 'z' then
         return True;
      elsif C >= '0' and then C <= '9' then
         return True;
      elsif C = '-' or else C = '_' or else C = '.' or else C = '~' then
         return True;
      else
         return False;
      end if;
   end Is_URL_Unreserved_Character;

   ----------------------
   -- Character tables --
   ----------------------

   type Character_Table is array (Character) of Boolean;

   function Create_URL_Character_Table return Character_Table is
   begin
      return R : Character_Table do
         for C in Character'Range loop
            R (C) :=
              Is_URL_Reserved_Character (C)
              or else Is_URL_Unreserved_Character (C);
         end loop;
      end return;
   end Create_URL_Character_Table;

   function Create_URL_Character_Or_Percent_Table return Character_Table is
   begin
      return R : Character_Table do
         for C in Character'Range loop
            R (C) :=
              Is_URL_Reserved_Character (C)
              or else Is_URL_Unreserved_Character (C) or else C = '%';
         end loop;
      end return;
   end Create_URL_Character_Or_Percent_Table;

   function Create_URL_Character_Or_Percent_Or_Multibyte_Table
     return Character_Table
   is
   begin
      return R : Character_Table do
         for C in Character'Range loop
            R (C) :=
              Is_URL_Reserved_Character (C)
              or else Is_URL_Unreserved_Character (C) or else C = '%'
              or else Character'Pos (C) >= 16#80#;
         end loop;
      end return;
   end Create_URL_Character_Or_Percent_Or_Multibyte_Table;

   URL_Character_Table : constant Character_Table :=
     Create_URL_Character_Table;
   URL_Character_Or_Percent_Table : constant Character_Table :=
     Create_URL_Character_Or_Percent_Table;
   URL_Character_Or_Percent_Or_Multibyte_Table : constant Character_Table :=
     Create_URL_Character_Or_Percent_Or_Multibyte_Table;

   ----------------------
   -- Is_URL_Character --
   ----------------------

   function Is_URL_Character (C : Character) return Boolean is
   begin
      return URL_Character_Table (C);
   end Is_URL_Character;

   ---------------------------------
   -- Is_URL_Character_Or_Percent --
   ---------------------------------

   function Is_URL_Character_Or_Percent (C : Character) return Boolean is
   begin
      return URL_Character_Or_Percent_Table (C);
   end Is_URL_Character_Or_Percent;

   ----------------------------------------------
   -- Is_URL_Character_Or_Percent_Or_Multibyte --
   ----------------------------------------------

   function Is_URL_Character_Or_Percent_Or_Multibyte
     (C : Character) return Boolean
   is
   begin
      return URL_Character_Or_Percent_Or_Multibyte_Table (C);
   end Is_URL_Character_Or_Percent_Or_Multibyte;

   ----------------
   -- URL_Encode --
   ----------------

   function URL_Encode (C : Character) return String is
      Hex_Digits : constant array (Unsigned_8 range 0 .. 15) of Character :=
        "0123456789ABCDEF";
      B : constant Unsigned_8 := Character'Pos (C);
   begin
      return R : String (1 .. 3) :=
        ('%', Hex_Digits (Shift_Right (B, 4)), Hex_Digits (B and 16#0f#));
   end URL_Encode;

   ----------------
   -- URL_Encode --
   ----------------

   function URL_Encode (S : Unbounded_String) return Unbounded_String is
   begin
      return R : Unbounded_String do
         for I in 1 .. Length (S) loop
            declare
               C : constant Character := Element (S, I);
            begin
               if Is_URL_Character (C) then
                  Append (R, C);
               else
                  Append (R, URL_Encode (C));
               end if;
            end;
         end loop;
      end return;
   end URL_Encode;

   -------------------------------
   -- URL_Encode_Except_Percent --
   -------------------------------

   function URL_Encode_Except_Percent
     (S : Unbounded_String) return Unbounded_String
   is
   begin
      return R : Unbounded_String do
         for I in 1 .. Length (S) loop
            declare
               C : constant Character := Element (S, I);
            begin
               if Is_URL_Character_Or_Percent (C) then
                  Append (R, C);
               else
                  Append (R, URL_Encode (C));
               end if;
            end;
         end loop;
      end return;
   end URL_Encode_Except_Percent;

   -----------------------
   -- Last_Scheme_Index --
   -----------------------

   function URL_Schemes_Colon_Index (S : Unbounded_String; First : Positive;
      Last : Natural; Schemes : URL_Scheme_Vectors.Vector) return Natural
   is
   begin
      for Scheme of Schemes loop
         if Starts_With (S, First, Last, Scheme) then
            declare
               Index : constant Natural := First + Scheme'Length;
            begin
               if Index <= Last and then Element (S, Index) = ':' then
                  return Index;
               end if;
            end;
         end if;
      end loop;
      return 0;
   end URL_Schemes_Colon_Index;

end Licana.URL_Utils;
