with Ada.Containers.Indefinite_Vectors;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;

package Licana.URL_Utils is

   function Is_URL_Character (C : Character) return Boolean with
      Inline;
   function Is_URL_Character_Or_Percent (C : Character) return Boolean with
      Inline;
   function Is_URL_Character_Or_Percent_Or_Multibyte
     (C : Character) return Boolean with
      Inline;
   function URL_Encode (C : Character) return String;
   function URL_Encode (S : Unbounded_String) return Unbounded_String;
   function URL_Encode_Except_Percent
     (S : Unbounded_String) return Unbounded_String;

   package URL_Scheme_Vectors is new Ada.Containers.Indefinite_Vectors
     (Positive, String);

   function URL_Schemes_Colon_Index (S : Unbounded_String; First : Positive;
      Last : Natural; Schemes : URL_Scheme_Vectors.Vector) return Natural;

end Licana.URL_Utils;
