package body Licana.Utils is

   --------------------------
   -- Escape_Tilda_And_Bar --
   --------------------------

   function Escape_Tilde_And_Bar (S : Unbounded_String) return Unbounded_String
   is
   begin
      return R : Unbounded_String do
         for I in 1 .. Length (S) loop
            declare
               C : Character := Element (S, I);
            begin
               if C = '~' or else C = '|' then
                  Append (R, '~');
               end if;
               Append (R, C);
            end;
         end loop;
      end return;
   end Escape_Tilde_And_Bar;

   --------------------
   -- Escape_Percent --
   --------------------

   function Escape_Percent (S : String) return Unbounded_String is
   begin
      return R : Unbounded_String do
         for C of S loop
            if C = '%' then
               Append (R, "%25");
            end if;
            Append (R, C);
         end loop;
      end return;
   end Escape_Percent;

end Licana.Utils;
