with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;

package Licana.Utils is

   function Escape_Tilde_And_Bar
     (S : Unbounded_String) return Unbounded_String;

   function Escape_Percent (S : String) return Unbounded_String;

end Licana.Utils;
