private with GNATCOLL.Traces;

package Licana is

   The_Version : constant String := "0.0.0";

private

   -------------
   -- Logging --
   -------------

   use GNATCOLL.Traces;

   Error_Logger : constant Trace_Handle := Create ("Licana_Error");
   Debug_Logger : constant Trace_Handle := Create ("Licana_Debug");

end Licana;
